<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function brand(){
    	return $this->belongsTo('App\Brand', 'brand_id', 'id');
    }
    public function category(){
    	return $this->belongsTo('App\Category', 'category_id', 'id');
    }
    public function avatarProduct(){
    	return $this->hasOne('App\Image','product_id','id')->where('status','1');
    }
    public function imageProduct(){
    	return $this->hasMany('App\Image','product_id','id')->where('status','2');
    }
}

