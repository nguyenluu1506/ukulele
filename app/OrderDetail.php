<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function product(){
    	return $this->hasOne('App\Product','id','product_id');
    }
    public function rating(){
    	return $this->hasOne('App\Rating','detail_id','id');
    }
}
