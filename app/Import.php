<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    public function request_import(){
    	//return $this->hasOne('App\Provider','id','provider_id');
    	return $this->hasOne('App\RequestImport','id','request_import_id');
    }
    public function detail(){
    	return $this->hasMany('App\ImportDetail','import_id','id');
    }
    public function product(){
    	return $this->belongsToMany('App\Product','import_details','import_id','product_id');
    }
    public function provider(){
    	return $this->hasOne('App\Provider','id','provider_id');
    }

	// suppliers									Import

	//     id - integer 							 request_import

 //    users 										RequestImport
	//     id - integer
	//     supplier_id - integer 						provider_id

	// history 									Provider
	//     id - integer
	//     user_id - integer 							id
} 
