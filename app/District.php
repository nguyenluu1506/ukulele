<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function Ward(){
    	return $this->hasMany('App\Ward','district_id','id');
    }
}
