<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function detail(){
    	return $this->hasMany('App\OrderDetail','order_id','id');
    }
    public function product(){
    	return $this->belongsToMany('App\Product','order_details','order_id','product_id');
    }
    public function user(){
    	return $this->hasOne('App\User','id','user_id');
    }
}
