<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestImport extends Model
{
    public function provider(){
    	return $this->hasOne('App\Provider','id','provider_id');
    }
    public function detail(){
    	return $this->hasMany('App\RequestImportDetail','request_import_id','id');
    }
    public function product(){
    	return $this->belongsToMany('App\Product','request_import_details','request_import_id','product_id');
    }
}
