<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public function District(){
    	return $this->hasMany('App\District','province_id','id');
    }
}
