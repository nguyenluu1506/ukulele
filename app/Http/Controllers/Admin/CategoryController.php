<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$category = Category::paginate(15);
    	return view('admin.category',compact('category'));
    }

    public function postAdd(Request $request){
    	$this->validate($request,[
            'name'=>'required',
	        ]);
    	$category = new Category();
    	$category->name = $request->name;
    	if($category->save()){
    	    return back();
        }
    }
    public function getEdit($id){
    	return $category = Category::where('id',$id)->first();
    }

    public function postEdit($id, Request $request){
    	$this->validate($request,[
            'name'=>'required',
	        ]);
    	$category = Category::where('id',$id)->first();
    	$category->name = $request->name;
        $category->save();
        return back();
    }
    public function delete($id){
    	if($category = Category::where('id',$id)->delete()){
    		return back();
    	}
    }
}
