<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Import;
use App\ImportDetail;
use App\Provider;
use App\Product;
use App\RequestImport;
use App\RequestImportDetail;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$import = Import::orderBy('id', 'desc')->paginate(15);
    	$request = RequestImport::where('status',0)->get();
    	$product = Product::get();
    	return view('admin.import',compact('import','request'));
    }

    //lấy toàn bộ SP
    public function getListProduct(){
        return Product::all();
    }

    // lấy ra để sửa
    public function getEdit($id){
        $import = Import::where('id',$id)->first();
        return [$import,$import->detail,$import->product,$import->request_import];
    }

    //thêm
    public function postAdd(Request $req){
    	$import = new Import();
        $import->request_import_id = $req->request_import;
    	$import->date = date("Y/m/d");
        $request = RequestImport::where('id',$req->request_import)->first();
        $request->status = 1;
        $request->save();
    	if($import->save()){
            for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new ImportDetail();
                $detail->import_id = $import->id;
                $detail->product_id = $req->product_id[$i];
                $detail->quantity_ok = $req->quantity_ok[$i];
                $detail->quantity_delivery = $req->quantity_delivery[$i];
                $detail->quantity_sum =$req->quantity_sum[$i];
                if($detail->save()){
                    $product = Product::where('id',$req->product_id[$i])->first();
                    $product->quantity += $req->quantity_ok[$i];
                    $product->save();
                }
            }
    		return redirect()->route('admin.import');
    	}
    }	

    //sửa
    public function postEdit($id, Request $req){
        // echo "<pre>";
        // print_r($req->all());
        // die;
        $import = Import::where('id',$id)->first();
        $detail = ImportDetail::where('import_id',$id)->get();
        for ($i=0; $i < count($detail); $i++) { 
            $product = Product::where('id',$req->product_id[$i])->first();
            $product->quantity -= $req->quantity_old[$i];
            $product->save();
        }
        ImportDetail::where('import_id',$id)->delete();
        for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new ImportDetail();
                $detail->import_id = $import->id;
                $detail->product_id = $req->product_id[$i];
                $detail->quantity_ok = $req->quantity_ok[$i];
                $detail->quantity_delivery = $req->quantity_delivery[$i];
                $detail->quantity_sum =$req->quantity_sum[$i];
                if($detail->save()){
                    $product = Product::where('id',$req->product_id[$i])->first();
                    $product->quantity += $req->quantity_ok[$i];
                    $product->save();
                }
            }
            return redirect()->route('admin.import');
    }


    //lấy SP từ phiếu yêu cầu
    public function getListProductRequest($id){
        $request = RequestImport::where('id',$id)->first();
        $provider = Provider::where('id',$request->provider_id)->first();
        return [$request,$request->detail, $request->product,$provider];
    }
}
