<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use App\OrderDetail;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }

    //home
    public function index(){
    	$export = Order::orderBy('id', 'desc')->paginate(15);
        $product = Product::get();
        $user = User::where('role',0)->get();
    	return view('admin.export',compact('export','product','user'));
    }


    //lấy ra sửa
    public function getEdit($id){
        $export = Order::where('id',$id)->first();
        return [$export,$export->detail,$export->product,$export->user];
    }

    //thêm
    public function postAdd(Request $req){
        $export = new Order();
        $export->date_order = date("Y/m/d");
        if($req->user_old && $req->new_user_name == ""){
            $user = User::where('id',$req->user_old)->first();
            $export->name_reciever = $user->name;
            $export->phone_reciever = $req->phone_reciever;
            $export->address_reciever = $req->address;
        }
        if($req->new_user_name != ""){
            $user = new User();
            $user->name = $req->new_user_name;
            $user->phone = $req->new_user_phone ? $req->new_user_phone : "";
            $user->address = $req->new_user_address ? $req->new_user_address : "Hà Nội";
            $user->ward_id = null;
            $user->district_id = null;
            $user->province_id = null;
            $user->password = null;
            $user->email = null;
            $user->role = 0;
            $user->remember_token = null;
            $user->save();
            $export->name_reciever = $req->new_user_name;
            $export->phone_reciever = $req->new_user_phone;
            $export->address_reciever = $req->new_user_address;
        }
        $export->user_id = $user->id;
        $export->sale_id = null;
        $export->status = 3;
        $export->payment = 'tien-mat';
        $export->order_tracking = null;
        if($export->save()){
            for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new OrderDetail();
                $detail->order_id = $export->id;
                $detail->product_id = $req->product_id[$i];
                $detail->price_sell = Product::where("id",$req->product_id[$i])->first()->price_sell;
                $detail->price_import = Product::where("id",$req->product_id[$i])->first()->price_import;
                $detail->quantity = $req->quantity[$i];
                $detail->is_rating = 0;
                if($detail->save()){
                    $product = Product::where('id',$req->product_id[$i])->first();
                    $product->quantity -= $req->quantity[$i];
                    $product->save();
                }
            }
            return redirect()->route('admin.export');
        }
    }

    //sửa
    public function postEdit($id, Request $req){
        // echo "<pre>";
        // print_r($req->all());
        // die;
        $export = Order::where('id',$id)->first();
        $export->sale_id = null;
        
        if(isset($req->order_tracking)){
            $export->status = 5;
        }else{
            $export->status = $req->status;
        }
        $export->payment = $req->payment_name;
        $export->order_tracking = $req->order_tracking;
        $export->phone_reciever = $req->phone_reciever;
        $export->address_reciever = $req->address;
        if($export->save()){
            $detail = OrderDetail::where('order_id',$id)->get();
            for ($i=0; $i < count($detail); $i++) { 
                $product = Product::where('id',$req->product_id[$i])->first();
                $product->quantity += $req->quantity_old[$i];
                $product->save();
            }
            OrderDetail::where('order_id',$id)->delete();
            for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new OrderDetail();
                $detail->order_id = $export->id;
                $detail->product_id = $req->product_id[$i];
                $detail->price_sell = Product::where("id",$req->product_id[$i])->first()->price_sell;
                $detail->price_import = Product::where("id",$req->product_id[$i])->first()->price_import;
                $detail->quantity = $req->quantity[$i];
                $detail->is_rating = 0;
                if($detail->save()){
                    $product = Product::where('id',$req->product_id[$i])->first();
                    $product->quantity -= $req->quantity[$i];
                    $product->save();
                }
            }
            return redirect()->route('admin.export');
        }
    }

    //xác nhận đơn hàng
    public function cf_ActiveExport($id){
        $export = Order::where('id',$id)->first();
        $export->status = 1;
        $export->save();
        return back();
    }
}
