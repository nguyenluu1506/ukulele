<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$slider = Slider::paginate(5);
    	return view('admin.slider',compact('slider'));
    }

    public function postAdd(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        // die;
    	$this->validate($request,[
            'file'=>'mimes:jpeg,jpg,png,JPEg,PNG,JPG,gif|max:10000',
            'title'=>'required',
	        ],[
	            'file.mimes'=>'File bạn chọn không đúng định dạng',
	            'file.max'=>'File bạn chọn quá nặng',
	        ]);
    	if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension('file');
	        if ($ext != 'jpg' && $ext !='png' && $ext !='PNG' && $ext !='JPG' && $ext !='JPEG' && $ext !='jpeg' && $ext !='gif' && $ext !='GIF') {
	            echo "<script> alert('Không đúng định dạng hình ảnh!'); </script>";;
	        }else{
	        	$slider = new Slider();
	    		$slider->title = $request->title;
	            $name = $file->getClientOriginalName();
	            $file->move('uploads/coverSlider/',$name);
	            $image = 'uploads/coverSlider/'.$name;
	            $slider->url = $image;
	            $slider->status = 1;
	            $slider->save();
	            return back();
            }
        }else{
        	$slider = new Slider();
	    	$slider->title = $request->title;
            $slider->url = null;
            $slider->status = 1;
            $slider->save();
            return back();
		}
		
    }
    public function confirm($id,$status){
    	$slider = Slider::where('id',$id)->first();
    	if ($status == 1) {
    		$slider->status = 0;
    	}
    	if($status == 0){
    		$slider->status =1;
    	}
    	$slider->save();
    	return back();
    }

}
