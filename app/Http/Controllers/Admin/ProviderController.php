<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Provider;

class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$provider = Provider::paginate(5);
    	return view('admin.provider',compact('provider'));
    }

    public function postAdd(Request $request){
    	$provider = new Provider();
    	$provider->name = $request->name;
    	$provider->phone = $request->phone;
    	$provider->address = $request->address;
    	$provider->save();
    	return back();
		
    }
    public function getEdit($id){
    	return $provider = Provider::where('id',$id)->first();
    }

    public function postEdit($id, Request $request){
    	$provider = Provider::where('id',$id)->first();
    	$provider->name = $request->name;
    	$provider->phone = $request->phone;
    	$provider->address = $request->address;
    	$provider->save();
    	return back();
    }
    public function delete($id){
    	if($provider = Provider::where('id',$id)->delete()){
    		return back();
    	}
    }
}
