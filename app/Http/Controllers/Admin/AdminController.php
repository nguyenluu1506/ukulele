<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Provider;
use App\Product;
use App\RequestImport;

class AdminController extends Controller
{
  	public function __construct()
	    {
	        $this->middleware('admin',['except' => 'logout']);
	    }
  	public function index()
  	{
	  	$export = Order::where('status',0)->get();
	  	$export_ok = Order::where("date_order",'=',date("Y-m-d"))->where('status',3)->get();
	  	$provider = Provider::get();
	  	$product = Product::get();
	    return view('admin.index',compact('export','provider','product','export_ok'));
  	}
}
