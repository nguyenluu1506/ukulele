<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class BrandController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$brand = Brand::paginate(4);
    	return view('admin.brand',compact('brand'));
    }

    public function postAdd(Request $request){
    	$this->validate($request,[
            'file'=>'mimes:jpeg,jpg,png,JPEg,PNG,JPG,gif|max:10000',
            'name'=>'required',
	        ],[
	            'file.mimes'=>'File bạn chọn không đúng định dạng',
	            'file.max'=>'File bạn chọn quá nặng',
	        ]);
    	if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension('file');
	        if ($ext != 'jpg' && $ext !='png' && $ext !='PNG' && $ext !='JPG' && $ext !='JPEG' && $ext !='jpeg' && $ext !='gif' && $ext !='GIF') {
	            echo "<script> alert('Không đúng định dạng hình ảnh!'); </script>";;
	        }else{
	        	$brand = new Brand();
	    		$brand->name = $request->name;
	            $name = $file->getClientOriginalName();
	            $file->move('uploads/coverBrand/',$name);
	            $image = 'uploads/coverBrand/'.$name;
	            $brand->coverImage = $image;
	            $brand->save();
	            return back();
            }
        }else{
        	$brand = new Brand();
	    	$brand->name = $request->name;
            $brand->coverImage = null;
            $brand->save();
            return back();
		}
		
    }
    public function getEdit($id){
    	return $brand = Brand::where('id',$id)->first();
    }

    public function postEdit($id, Request $request){
    	$brand = Brand::where('id',$id)->first();
    	$brand->name = $request->name;
    	$file = $request->file('file');
    	if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension('file');
            $ext = $file->getClientOriginalExtension('file');
	        if ($ext != 'jpg' && $ext !='png' && $ext !='PNG' && $ext !='JPG' && $ext !='JPEG' && $ext !='jpeg' && $ext !='gif' && $ext !='GIF') {
	            echo "<script> alert('Không đúng định dạng hình ảnh!'); </script>";;
	        }else{
	            $name = $file->getClientOriginalName();
	            $file->move('uploads/coverBrand/',$name);
	            $image = 'uploads/coverBrand/'.$name;
	            $brand->coverImage = $image;
	            $brand->save();
	            return back();
            }
        }else{
            $brand->save();
            return back();
		}
    }
    public function delete($id){
    	if($brand = Brand::where('id',$id)->delete()){
    		return back();
    	}
    }
}
