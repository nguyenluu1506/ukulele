<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Provider;
use App\Product;
use App\RequestImport;
use App\RequestImportDetail;
use Mail;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$request = RequestImport::paginate(10);

    	$provider = Provider::get();
    	$product = Product::get();
    	// echo "<pre>";
    	// foreach ($request as $key => $value) {
    		
    	// 	//foreach ($value->product as $key1 => $val) {
    	// 		print_r($value->detail);
    	// //	}
    	// }die;
    	return view('admin.request',compact('request','provider','product'));
    }

    public function postAdd(Request $req){
        $request = new RequestImport();
        $request->provider_id = $req->provider;
        $request->status = 0; //0: vua moi yeu cau; 1: da nhap ;
        if ($request->save()) {
            for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new RequestImportDetail();
                $detail->request_import_id = $request->id;
                $detail->product_id = $req->product_id[$i];
                $detail->quantity = $req->quantity[$i];
                $detail->save();
            }
        }
        $data = ['id'=>$request->id,'product'=>$request->product, 'quantity'=>$request->detail];
        Mail::send('mails.request-import',$data, function($msg){
            $msg->from('nluu1235@gmail.com','Chủ cửa hàng nhạc cụ TNGUITAR');
            $msg->to('nluu246@gmail.com','Nhà cung cấp')->subject("Phiếu yêu cầu nhập đơn hàng mới");
    	});
    	return redirect()->route('admin.request');
	}
    public function getEdit($id){
    	$request = RequestImport::where('id',$id)->first();
    	$product = Product::get();
    	return [$request,$request->detail,$request->product,$product];
    }
    public function postEdit(Request $req,$id){
    	$request = RequestImport::where('id',$id)->first();
    	RequestImportDetail::where('request_import_id',$id)->delete();
    	$request->provider_id = $req->provider;
        $request->status = 0; //0: vua moi yeu cau; 1: da nhap ;
        if ($request->save()) {
            for ($i=0; $i < count($req->product_id); $i++) { 
                $detail = new RequestImportDetail();
                $detail->request_import_id = $request->id;
                $detail->product_id = $req->product_id[$i];
                $detail->quantity = $req->quantity[$i];
                $detail->save();
            }
        }
        return redirect()->route('admin.request');
    }
}
