<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\OrderDetail;
class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }

    //báo cáo mặc định: theo đơn hàng
    public function index(){
  //   	$date = "2010-08-12";
		// $d = date_parse_from_format("Y-m-d", $date);
		// echo $d["month"];

		$currentMonth = date('m');
		$data = Order::whereRaw('MONTH(created_at) = ?',[$currentMonth])->get();
    	//$export = Order::where("date_order",date('m'))->get()->count();
    	return view('admin.report',compact('data'));
    }
    public function postMonth(Request $req)
    {
    	$month = $req->mois;
    	$data = Order::whereRaw('MONTH(created_at) = ?', $req->mois)->get();
    	return view('admin.report',compact('data','month'));
    }
    public function getReport($type){
        if ($type == 4) {
            $data = Order::where('status',4)->get(); 
        }
        if ($type == 5) {
            $data = Product::where('quantity','<=',10)->orderBy('quantity', 'ASC')->get();
        }
        if ($type == 6) {
            $data = OrderDetail::groupBy('product_id')->selectRaw('product_id, sum(quantity) as sum')->orderBy('sum', 'DESC')->get(10);
        }
        $status = $type;
            return view('admin.report',compact('data','status'));
    }
}
