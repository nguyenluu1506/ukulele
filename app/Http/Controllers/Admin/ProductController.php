<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Brand;
use App\Category;
use App\Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
        $product = Product::paginate(7);
        $category = Category::all();
        $brand = Brand::all();
        return view('admin.product',compact('product','category','brand'));
    }
    public function postAdd(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        // die;
        $tag = "";
        for($i = 0; $i < sizeof($request->tag); $i++){
            $tag .= $request->tag[$i] .", ";
        }
    	$this->validate($request,[
            'name'=>'required',
	        ]);
    	$product = new Product();
    	$product->name = $request->name;
    	$product->description = $request->description;
    	$product->quantity = $request->quantity;
    	$product->price_sell = $request->price_sell;
    	$product->price_import = $request->price_import;
    	$product->brand_id = $request->brand;
    	$product->category_id = $request->category;
        $product->tag = $tag;
    	$product->status = 1;
    	if($product->save()){
    	    if ($request->hasFile('product_avt')) {
            $file = $request->file('product_avt');
            $ext = $file->getClientOriginalExtension('product_avt');
	        if ($ext != 'jpg' && $ext !='png' && $ext !='PNG' && $ext !='JPG' && $ext !='JPEG' && $ext !='jpeg' && $ext !='gif' && $ext !='GIF') {
	            echo "<script> alert('Không đúng định dạng hình ảnh!'); </script>";;
	        }else{
		        	$img = new Image();
		    		$img->product_id = $product->id;
		            $name = $file->getClientOriginalName();
		            $file->move('uploads/product_avt/',$name);
		            $image = 'uploads/product_avt/'.$name;
		            $img->url = $image;
		            $img->type = "product_avt";
		            $img->status = 1;
		            $img->save();
	            }
        	} 
        	if ($request->hasFile('product_img')) {
        		foreach ($request->product_img as $key => $value) {
		        	$img = new Image();
		    		$img->product_id = $product->id;
		            $name = mt_rand(1000, 2499);
		            $value->move('uploads/product_img/',$name);
		            $image = 'uploads/product_img/'.$name;
		            $img->url = $image;
		            $img->type = "product_img";
		            $img->status = 2;
		            $img->save();
	            }
        	}
        }
        return back();
    }
    public function cfActive($id,$status){
    	$product = Product::where('id',$id)->first();
    	if($status == 1){
    		$product->status = 0;
    	}else{
    		$product->status = 1;
    	}
    	if($product->save()){
    		return $product->status;
    	}
    }
    public function getEdit($id){
    	$product = Product::where('id',$id)->first();
    	return [$product, $product->brand, $product->category, $product->avatarProduct,$product->imageProduct];
    }
    public function deleteImg($id){
    	return Image::where('id',$id)->delete();
    }
    public function postEdit($id, Request $request){
    	$this->validate($request,[
            'name'=>'required',
	        ]);
        $tag = "";
        for($i = 0; $i < sizeof($request->tag); $i++){
            $tag .= $request->tag[$i] .", ";
        }
    	$product = Product::where('id',$id)->first();
    	$product->name = $request->name;
    	$product->description = $request->description;
    	$product->quantity = $request->quantity;
    	$product->price_sell = $request->price_sell;
    	$product->price_import = $request->price_import;
    	$product->brand_id = $request->brand;
    	$product->category_id = $request->category;
        $product->tag = $tag;
    	$product->status = 1;
    	if($product->save()){
    	    if ($request->hasFile('product_avt')) {
            $file = $request->file('product_avt');
            $ext = $file->getClientOriginalExtension('product_avt');
	        if ($ext != 'jpg' && $ext !='png' && $ext !='PNG' && $ext !='JPG' && $ext !='JPEG' && $ext !='jpeg' && $ext !='gif' && $ext !='GIF') {
	            echo "<script> alert('Không đúng định dạng hình ảnh!'); </script>";;
	        }else{
		        	Image::where('product_id',$product->id)->where('type','product_avt')->delete();
		        	$img = new Image();
		    		$img->product_id = $product->id;
		            $name = $file->getClientOriginalName();
		            $file->move('uploads/product_avt/',$name);
		            $image = 'uploads/product_avt/'.$name;
		            $img->url = $image;
		            $img->type = "product_avt";
		            $img->status = 1;
		            $img->save();
	            }
        	} 
        	if ($request->hasFile('product_img')) {
        		foreach ($request->product_img as $key => $value) {
		        	$img = new Image();
		    		$img->product_id = $product->id;
		            $name = mt_rand(1000, 2499).$value->getClientOriginalExtension($value);
		            $value->move('uploads/product_img/',$name);
		            $image = 'uploads/product_img/'.$name;
		            $img->url = $image;
		            $img->type = "product_img";
		            $img->status = 2;
		            $img->save();
	            }
        	}
        }
        return back();
    }
    public function delete($id){
    	if($product = Product::where('id',$id)->delete()){
    		return back();
    	}
    }
}
