<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => 'logout']);
    }
    public function index(){
    	$user = User::paginate(5);
    	return view('admin.user',compact('user'));
    }
    public function getEdit($id){
    	return $user = User::where('id',$id)->first();
    }

    public function postEdit($id, Request $request){
    	$user = User::where('id',$id)->first();
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->phone = $request->phone;
    	$user->address = $request->address;
    	$user->role = $request->role;
    	if($user->save()){
    		return back();
    	}
    	
    }
    public function delete($id){
    	if($user = User::where('id',$id)->delete()){
    		return back();
    	}
    }
}
