<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\District;
use App\Ward;
use App\Province;
use App\Order;
use App\OrderDetail;
use Cart;
use Auth;
use Mail;

class CheckoutController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }
    public function index(){
    	$user = Auth()->user();
    	$province = Province::get();
    	$cart = Cart::content();
        $total = Cart::subtotal();
    	return view('frontend.checkout',compact('user','province','cart','total'));
    }
    public function getDistrict($id){
        return District::where('province_id',$id)->get();
    }
    public function getWard($id){
    	return Ward::where('district_id',$id)->get();
    }
    public function postCheckout(Request $req){
    	$ward = Ward::where('id',$req->ward_reciever)->first()->name;
    	$district = District::where('id',$req->district_reciever)->first()->name;
    	$province = Province::where('id',$req->province_reciever)->first()->name;
    	$order = new Order();
    	$cart = Cart::content();
    	$order->user_id = Auth()->user()->id;
    	$order->date_order = date("Y/m/d h:m:s");
    	$order->name_reciever = $req->name_reciever;
    	$order->phone_reciever = $req->phone_reciever;
    	$order->address_reciever = $req->address_reciever.", ".$ward.", ".$district.", ".$province;
    	$order->order_tracking = "";
    	$order->payment = $req->payment_name;
    	$order->status = "0"; //0:, 1:đat thanh cong, 2:thanh toan thanh cong, 3:giao hàng thành công
    	if($order->save()){
    		foreach($cart as $key => $value){
    			$detail = new OrderDetail();
    			$detail->product_id = $value->id;
    			$detail->order_id = $order->id;
    			$detail->price_import = $value->options->price_import;
    			$detail->price_sell = $value->price;
    			$detail->quantity = $value->qty;
                $detail->is_rating = 0;
    			$detail->save();
    		}
            Cart::destroy();
            $result = Order::where('id',$order->id)->first();
            $detail_mail = OrderDetail::where('order_id',$result->id)->get();
            $data = ['order'=>$result,'product'=>$detail_mail];
            Mail::send('mails.booking-success',$data, function($msg){
                $msg->from('nluu246@gmail.com','Chủ cửa hàng TNGUITAR');
                $msg->to('nluu246@gmail.com','Chủ cửa hàng')->subject('Thông báo đặt hàng thành công!');
            });
            return view('frontend.checkout-success',compact('result'));
    	}
    }
}
