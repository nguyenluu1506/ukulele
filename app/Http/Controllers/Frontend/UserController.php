<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Province;
use App\User;
use App\Order;
use App\OrderDetail;
use App\Rating;
use App\Product;

class UserController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }
    public function index(){
    	$user = Auth()->user();
    	$province = Province::get();
    	return view('frontend.user-infor',compact('user','province'));
    }
    public function postEdit(Request $request){
    	$user = User::where('id',Auth()->user()->id)->first();
    	$user->name = $request->name;
    	$user->phone = $request->phone;
    	$user->address = $request->address;
    	$user->province_id = $request->province;
    	$user->district_id = $request->district;
    	$user->ward_id = $request->ward;
    	$user->save();
    	return back()->with(['success'=>'Sửa thông tin thành công!']);
    }
    public function getBill(){
    	$bill = Order::where('user_id',Auth()->user()->id)->get();
    	return view('frontend.user-bill',compact('bill'));
    }
    public function getBillDetail($id){
        $order = Order::where('id',$id)->first();
        // echo "<pre>";
        // print_r($order->status);
        // die;
    	$bill = OrderDetail::where('order_id',$id)->get();
    	return view('frontend.user-bill-detail',compact('bill','order'));
    }
    public function cancelOrder($id){
        $order = Order::where('id',$id)->first();
        $order->status = 4;
        $order->save();
        return redirect()->route('frontend.user.getBill');
    }
    
    public function postRating(Request $req){
        // echo "<pre>";
        // print_r($req->product_id);
        // die;
        $product = Product::where('id',$req->product_id)->first();
        // echo "<pre>";
        // print_r($product->rating+$req->star);
        // die;
        $product->rating = ($product->rating+$req->star)/(2);
        $product->count_rating += 1;
        $product->save();
        $detail = OrderDetail::where('id',$req->detail_id)->first();
        $detail->is_rating = 1;
        $detail->save();
        $rating = new Rating();
        $rating->rating = $req->star;
        $rating->comment = $req->comment;
        $rating->product_id = $req->product_id;
        $rating->detail_id = $req->detail_id;
        $rating->user_id = Auth()->user()->id;

        $rating->save();
        return back();
    }
}
