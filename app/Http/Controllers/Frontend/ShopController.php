<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Brand;
use App\Product;
use App\Rating;
use Cart;


class ShopController extends Controller
{
	public function __construct(){
        $category = Category::all();
    	$brand = Brand::all();
        view()->share('brand',$brand);
        view()->share('category',$category);
    }

    public function index(){
    	$product = Product::paginate(9);
    	$a = "abc";
    	return view('frontend.shop',compact('category','brand','product','a'));
    }

    public function getProductDetail($id){
    	$product = Product::where('id',$id)->first();
        $rating = Rating::where('product_id',$id)->get();
    	return view('frontend.detail',compact('product','brand','rating'));
    }

    public function getProductByBrandId($id){
    	$product = Brand::where('id',$id)->first()->Product;
    	return view('frontend.shop',compact('category','brand','product'));
    }

    public function getProductByCategoryId($id){
    	$product = Category::where('id',$id)->first()->Product;
    	return view('frontend.shop',compact('category','brand','product'));
    }

    public function addCart($id){
    	$product = Product::where('id',$id)->first();
    	if(Cart::add(array('id'=>$id, 'name'=>$product->name, 'qty'=>1, 'price'=>$product->price_sell, 'options' => ['img' => $product->avatarProduct->url, 'price_import' => $product->price_import]))){
    		return Cart::count();
    	}

    }
    function getQtyCart(){
    	return Cart::count();
    }
    public function getCart(){
    	$cart = Cart::content();
        $total = Cart::subtotal();
        $tax = Cart::tax();
        /*echo "<pre>";
        print_r(count($cart));
        die;*/
        return view('frontend.cart',compact('cart','total','tax'));
    }
    public function updateCartUp($id, $qty){
        Cart::update($id,$qty);
        return [Cart::content()[$id]->price*Cart::content()[$id]->qty, Cart::subtotal(), Cart::count() ];
    }

    public function updateCartDown($id, $qty){
        if($qty !=0){
            Cart::update($id,$qty);
         	return [Cart::content()[$id]->price*Cart::content()[$id]->qty, Cart::subtotal(), Cart::count()];
        }
        else{
            Cart::remove($id);
         	return  [Cart::subtotal(),Cart::count()];
        }
    }
    public function delItemInCart($id){
    	Cart::remove($id);
        return Cart::subtotal();
    }

    public function updateCartInput($id, $qty){
        Cart::update($id,$qty);
        return [Cart::content()[$id]->price*Cart::content()[$id]->qty, Cart::subtotal(), Cart::count() ];
    }

}
