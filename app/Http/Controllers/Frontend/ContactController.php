<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class ContactController extends Controller
{
    public function index(){
    	return view('frontend.contact');
    }
    public function postContact(Request $req){
    	// $email = $req->email;
    	// $subject = $req->subject;
        $data = ['name'=>$req->name,'email'=>$req->email, 'messages'=>$req->message];
        Mail::send('mails.contact',$data, function($msg){
            $msg->from('nluu246@gmail.com','Khách hàng');
            $msg->to('nluu246@gmail.com','Chủ cửa hàng')->subject('Góp ý');
    	});
    	return view('frontend.contact')->with('success', "Cảm ơn bạn đã phản hồi!");
    }


}
