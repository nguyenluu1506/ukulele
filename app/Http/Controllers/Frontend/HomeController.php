<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Brand;
use App\Product;
use App\Slider;

class HomeController extends Controller
{
    public function index(){
    	$category = Category::all();
    	// echo "<pre>";
    	// foreach ($category as $value) {
    	// 	foreach ($value->Product as $val) {
    	// 		print_r($val->avatarProduct);
    	// 	}
    	// }
    	// die;
    	$brand = Brand::all();
    	$product = Product::paginate(6);
    	$slider = Slider::where('status','1')->get();
    	return view('frontend.index',compact('category','brand','product','slider'));
    }
}
