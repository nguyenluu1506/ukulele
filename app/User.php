<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Province()
    {
        return $this->belongsTo('App\Province','province_id','id');
    }
    public function District()
    {
        return $this->belongsTo('App\District','district_id','id');
    }
    public function Ward()
    {
        return $this->belongsTo('App\Ward','ward_id','id');
    }
}
