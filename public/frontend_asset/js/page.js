$(document).ready(function() {
	$.ajax({
		url: "getQtyCart",
		type: 'GET',
		success:function(data){
			$("#number-item").html(data);
		}
	});


	$(".cart_quantity_up").click(function(event) {
		var url = "updateCartUp/"; 
		var id = $(this).attr('id');
		var qty = $(this).parent().find('#qty_'+id).val();
		qty = parseInt(qty) + 1 ;
		$("#qty_"+id).val(qty);
		$.ajax({
			url: url+id+"/"+qty,
			type: 'GET',
			success:function(data){
				$("#total_"+id).val(data[0]);
				//$("#subtotal").val(data[1]);
				$("#subtotal").val(data[1].substring(0,data[1].length-3)+" VNĐ");
				$("#number-item").html(data[2]);
			}
		});
	});

	
	$(".cart_quantity_input").change(function() {
		if (confirm("Bạn có chắc thay đổi này chứ?")) {
			var url = "updateCartInput/"; 
			var id = $(this).attr('id');
			var qty = $(this).val();
			$("#qty_"+id).val(qty);
			$.ajax({
				url: url+id+"/"+qty,
				type: 'GET',
				success:function(data){
					$("#total_"+id).val(data[0]);
					//$("#subtotal").val(data[1]);
					$("#subtotal").val(data[1].substring(0,data[1].length-3)+" VNĐ");
					$("#number-item").html(data[2]);
				}
			});
		}
		
	});

	$(".cart_quantity_down").click(function() {
		var url = "updateCartDown/"; 
		var id = $(this).attr('id');
		var qty = $(this).parent().find('#qty_'+id).val();
		qty = parseInt(qty) - 1 ;
		$("#qty_"+id).val(qty);
		if($("#qty_"+id).val()==0){
			$("#item_"+id).remove();
			$.ajax({
			url: url+id+"/"+qty,
			type: 'GET',
			success:function(data){	
				//$("#subtotal").val(data[0]);
				$("#subtotal").val(data[0].substring(0,data[0].length-3)+" VNĐ");
				$("#number-item").html(data[1]);
			}
		});
		}else{
			$.ajax({
			url: url+id+"/"+qty,
			type: 'GET',
			data: {'id':id,'qty':qty},
			success:function(data){
				$("#total_"+id).val(data[0]);
				//$("#subtotal").val(data[1]);
				$("#subtotal").val(data[1].substring(0,data[1].length-3)+" VNĐ");	
				$("#number-item").html(data[2]);
			}
		});
		}
	});



	$("#province_reciever").change(function() {
		var url = "getDistrict/"; 
		var id = $(this).parent().find('#province_reciever').val();
			$.ajax({
			url: url+id,
			type: 'GET',
			success:function(data){		
				$('#district_reciever').empty();
				$('#ward_reciever').empty();
				$( "#district_reciever" ).append( "<option > Quận/Huyện </option>" );
				$('#ward_reciever').append('<option > Phường/Xã </option>');
				for(var i = 0 ; i < data.length; i++){
					$( "#district_reciever" ).append( "<option id=' "+ data[i].id+ " ' value=' "+data[i].id+"'>"+ data[i].name +"</option>" );
				}
			}
		});
	});


	$("#district_reciever").change(function() {
		var url = "getWard/"; 
		var id = $(this).parent().find('#district_reciever').val();
			$.ajax({
			url: url+id,
			type: 'GET',
			success:function(data){		
				$('#ward_reciever').empty();
				$( "#ward_reciever" ).append( "<option > Phường/Xã </option>" );
				for(var i = 0 ; i < data.length; i++){
					$( "#ward_reciever" ).append( "<option value=' "+data[i].id+"'>"+ data[i].name +"</option>" );
					
				}
			}
		});
	});

	$(".cart_quantity_delete").click(function() {
		var url = "delItemInCart/";
		var rowId = $(this).attr('id');
		$.ajax({
			url: url+rowId,
			type: 'GET',
			success: function(data){
					$("#item_"+rowId).remove();
					// $("#subtotal").val(data);
					$("#subtotal").val(data.substring(0,data.length-3)+" VNĐ");
				}			
		});
		
	});

	$("#check").click(function(event) {
		if($("#check").is(':checked')){
			$("#name_reciever").val($("#name").val());
			$("#phone_reciever").val($("#phone").val());
			$("#address_reciever").val($("#address").val());
			$('#province_reciever option[value='+$("#province").val()+']').attr('selected','selected');
			$('#district_reciever').append( "<option value=' "+$("#district").val()+"' selected>"+ $("#district").find('option').text() +"</option>" );
			$('#ward_reciever').prepend( "<option value=' "+$("#ward").val()+"' selected>"+ $("#ward").find('option').text() +"</option>" );
		}
		else{
			$("#name_reciever").val();
			$("#phone_reciever").val();
			$("#address_reciever").val();
			$('#province_reciever option:selected').removeAttr("selected");
			$('#district_reciever').empty();
			$('#ward_reciever').empty();
			$( "#district_reciever" ).append( "<option > Quận/Huyện </option>" );
			$( "#ward_reciever" ).append( "<option > Phường/Xã </option>" );

		}
	});
});

function addCart(id){
	var url = "addCart/"+id;
		$.ajax({
			url: url,
			type: 'GET',
			success: function(data){
				$.notify("Thêm giỏ hàng thành công!","success");
				$("#number-item").html(data);
			}
		});

}
function rating(star){
    var star = star;
    $("#submit").removeAttr('disabled');
    if($("i[name=star"+star+"]").hasClass('fa-star-o')){
        for(var i = 1; i<= star; i++){
            $("i[name=star"+i+"]").removeClass('fa-star-o').addClass('fa-star');
        }
    }
    else{
        for(var i = 5; i > star; i--){
            $("i[name=star"+i+"]").removeClass('fa-star').addClass('fa-star-o');
        }
    }
    $("#star").val(star);
}
function setVal(id,product_id){
	$("#detail_id").val(id);
	$("#product_id").val(product_id);
}

