
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#view_Img')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150)
                    .removeAttr("hidden");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function getDetail(id,table){
        $("#product_get").empty();
        $.ajax({
            url: 'admin/'+table+'/getDetail/'+id,
            type: 'get',
            success:function(data){
                $("#time").html(data[0]['date']);
                $("#times").html(data[0]['date_order']);
                $("#user_get").html(data[3].name);
                $("#phone").html(data[3].phone);
                $("#address").html(data[3].address);
                $("#provider_get").html(data[3]);
                for (var i = 0; i < data[2].length; i++) {
                    //for (var j = 0; j < data[3].length; j++) {
                        // console.log(data[3][j].id);
                        //  console.log(data[3][i].name);
                        // if (data[2][i].id == data[3][i].id) {
                            $("#product_get").append("<tr><td>"+data[2][i].name+"</td><td >"+data[1][i].quantity+"</td><td >"+data[1][i].price_import+"</td><td >"+data[1][i].price_sell+"</td><td >"+data[1][i].price_sell*data[1][i].quantity+"</td></tr>");
                        //}
                    //}
                    
                }
                console.log(data);
                
            }
        });
    }
    
    function cf_Active(id,status){
        console.log($(this));
        $.ajax({
            url: 'admin/product/cfActive/'+id+'/'+status,
            type: 'get',
            success:function(data){
                if(data == 1){
                    $(this).parent().empty();
                    $(this).parent().html('<a class="btn btn-xs btn-info" href="javascript:void(0);" onclick="cf_Active('+id+','+data+')" >Ẩn</a>');
                }else{
                    $(this).parent().empty();
                   $(this).parent().html('<a class="btn btn-xs btn-success" href="javascript:void(0);" onclick="cf_Active('+id+','+data+')">Hiện</a>');
                }
            }
        });
    }

    
    function add(table){
        $(".modal-title").text("Thêm thông tin");
        $("#frm").attr("action","admin/"+table+"/postAdd");
        
        resetModal();
    }
    function setDataClientProduct(data){
        // console.log(data);
        $("#name").val(data[0].name);
        $("#summernote").summernote('code',data[0].description);
        //$("#description").val(data[0].description);
        $("#quantity").val(data[0].quantity);
        $("#price_sell").val(data[0].price_sell);
        $("#price_import").val(data[0].price_import);
        var arr = (data[0].tag).split(',');
        for(var i = 0; i< arr.length;i++){
            console.log($(arr[i]).attr('checked', 'true'));
            $(arr[i]).prop('selected', 'true');
        }
        $('#brand option[value='+data[1].id+']').attr('selected','selected');
        $('#category option[value='+data[2].id+']').attr('selected','selected');
        $('#view_Img')
        .attr('src',data[3].url)
        .width(150)
        .height(150)
        .removeAttr("hidden");
        for( var i =0 ; i<data[4].length; i++){
            $("#product_img").append('<div class="col-md-2  thum "><img src='+data[4][i].url+' width="100%" height="100%"> <span class="remove_img_preview"  onclick="deleteImg('+data[4][i].id+')"></span>');
        }
    }
    
    function deleteImg(id){
        $.ajax({
            url: 'admin/product/deleteImg/'+id,
            type: 'get',
            success:function(data){
                console.log(data);
            }
        });
    }
    function setDataClientCategory(data){
        $("#name").val(data.name);
    }
    function setDataClientBrand(data){
        $("#name").val(data.name);
        $("#title").val(data.title);
        $('#view_Img')
            .attr('src',data.coverImage)
            .width(150)
            .height(150)
            .removeAttr("hidden");
    }

    function setDataClientProvider(data){
        $("#name").val(data.name);
        $("#phone").val(data.phone);
        $("#address").val(data.address);
    }
    function setDataClientUser(data){
        $("#name").val(data.name);
        $("#email").val(data.email);
        $("#phone").val(data.phone);
        $("#address").val(data.address);
        if(data.role == 1){
            $('#role option[value=1]').attr('selected','selected');
        }else{
            $('#role option[value=0]').attr('selected','selected');
        }
    }

    //getEdit RequestImport
    function setDataClientRequest(data){
        $("#listProduct").empty();
        window.option = "";
        $('#provider option[value='+data[0].provider_id+']').attr('selected','selected');
        for (var i = 0; i < data[3].length; i++) {
            window.option += '<option value="'+data[3][i].id+'">'+data[3][i].name+'(Số lượng:'+data[3][i].quantity+')</option>';
            
        }
        if (data[0].status == 0) {
            $("#them").removeClass('hidden');
            $("#btnAdd").removeClass('hidden');
            for (var i = 0; i < data[1].length; i++) {
                window.option += '<option value="'+data[2][i].id+'">'+data[2][i].name+'(Số lượng:'+data[2][i].quantity+')</option>';
                if (data[1][i].product_id == data[2][i].id) {
                    window.option += '<option value="'+data[2][i].id+'" selected>'+data[2][i].name+'(Số lượng:'+data[2][i].quantity+')</option>'; 
                }
                $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-6"><select required="true" class="form-control arrayProduct"  name="product_id[]" id="" class="getQty"><option value="">Chọn sản phẩm</option>'+option+'</select></div><div class="col-md-5"><input type="text" class="form-control" id="quantity" name="quantity[]" required="true" value="'+data[1][i].quantity+'" placeholder="Nhập số lượng" width="100%"></div><div class="col-md-1 del"><a href="javascript:void(0)" class="btn btn-danger" >-</a></div></div>');
            }
        }else{
            $("#them").addClass('hidden');
            $("#btnAdd").addClass('hidden');
            for (var i = 0; i < data[1].length; i++) {
                window.option += '<option value="'+data[2][i].id+'">'+data[2][i].name+'(Số lượng:'+data[2][i].quantity+')</option>';
                if (data[1][i].product_id == data[2][i].id) {
                    window.option += '<option value="'+data[2][i].id+'" selected>'+data[2][i].name+'(Số lượng:'+data[2][i].quantity+')</option>'; 
                }
                $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-6"><select class="form-control arrayProduct" disabled="true" name="product_id[]" id="" class="getQty"><option value="">Chọn sản phẩm</option>'+option+'</select></div><div class="col-md-5"><input type="text" class="form-control" id="quantity" name="quantity[]" value="'+data[1][i].quantity+'" placeholder="Nhập số lượng" readonly width="100%"></div></div>');
            }
            $("#listProduct").append('<i style="color:red;">Bạn không thể sửa vì đơn hàng đã được nhập kho!</i>');
        }
        
    }

    //getEdit Export
    function setDataClientExport(data){
        $("#order_status").removeClass('hidden');
        $("#them").addClass('hidden');  
        $("#tracking").removeClass('hidden');
        $("#listProduct").empty();
        $("#new").addClass('hidden');
        $('#user_old option[value='+data[3].id+']').attr('selected','selected');

        //
        $('#user_old').attr('disabled', 'true');
        $("#address").attr('readonly', 'true');
        $("#phone_reciever").attr('readonly', 'true');
        //
        $("#address").val(data[0].address_reciever);
        $('input:radio[name=payment_name]').filter('[value='+data[0].payment+']').prop('checked', true);
        $('#status option[value='+data[0].status+']').attr('selected','selected');
        $("#order_tracking").val(data[0].order_tracking);
        $("#phone_reciever").val(data[0].phone_reciever);
        var quantity_ok = "";
        var select = "";  
        var quantity_old="";
        for(var i = 0; i< data[1].length; i++){
            if(data[1][i].product_id == data[2][i].id){
                quantity_old +='<input  type="number" style="margin-top:5px;" class="form-control hidden" id="quantity_old" min="0" name="quantity_old[]" value="'+data[1][i].quantity+'" width="100%"></td></tr>';
                quantity_ok += '<tr><th>Số lượng mua: </th><td> <input required="true" type="number" style="margin-top:5px;" class="form-control" readonly id="quantity" min="0" name="quantity[]" required="true" placeholder="SL mua" max="'+(data[2][i].quantity)+'" value="'+data[1][i].quantity+'" width="100%"></td></tr>';
                select += '<select required="true" disabled="true" class="form-control arrayProduct" style="margin-top:5px;" name="product_id[]" id="" class="getQty"><option value="'+data[1][i].product_id+'">'+data[2][i].name+'</option><select>';
            }
        }
        $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-6">'+select+'</div><div class="col-md-6 text-center" ><table width="100%">'+quantity_ok+'</table></div><div>'+quantity_old+'</div></div>');
    }



    //getEdit Import
    function setDataClientImport(data){
        $('#request_import').attr({
            disabled: 'true'
        });
        $("#listProduct").empty();
        //$("#listProduct").parent().append('<div class="form-group"><div class="col-sm-3"></div><div class="col-sm-9" style="padding-left: 23px;"><a href="javascript:void(0)" onclick="addList()"><i class="fa fa-plus-square" aria-hidden="true"></i> Thêm</a></div></div>');
        var option = '<option value="'+data[3].id+'" selected>RQ-'+data[3].id+'</option>'; 
        var quantity_ok = "";
        var quantity_old = "";
        var quantity_sum = "";
        var quantity_delivery ="";
        var select = "";  
        $('#request_import').append(option);
        for(var i = 0; i< data[1].length; i++){
            // console.log(arrayIdProduct, arrayIdProduct.indexOf(data[i].id.toString()), data[i].id);
            if(data[1][i].product_id == data[2][i].id){
                quantity_old += '<input  type="number" style="margin-top:5px;" class="form-control hidden" id="quantity_old" min="0" name="quantity_old[]" value="'+data[1][i].quantity_ok+'" width="100%"></td></tr>';
                quantity_ok += '<tr><th>SL đạt yêu cầu: </th><td> <input required="true" type="number" style="margin-top:5px;" class="form-control" id="quantity_ok" min="0" name="quantity_ok[]" required="true" placeholder="SL thực tế có thể nhập" max="'+(data[1][i].quantity_fail + data[1][i].quantity_ok)+'" value="'+data[1][i].quantity_ok+'" width="100%"></td></tr>';
                quantity_delivery += '<tr><th>SL giao tới: </th><td> <input required="true" type="number" style="margin-top:5px;" class="form-control" id="quantity_delivery" min="0" name="quantity_delivery[]" required="true"  value="'+data[1][i].quantity_delivery+'" width="100%"></td></tr>';
                quantity_sum += '<tr><th>SL yêu cầu: </th><td><input style="margin-top:5px;" readonly type="text" class="form-control " id="quantity_sum" name="quantity_sum[]" value="'+(data[1][i].quantity_sum)+'"  width="100%"></td></tr>';
                select += '<select required="true"  class="form-control arrayProduct" style="margin-top:5px;" name="product_id[]" id="" class="getQty"><option value="'+data[1][i].product_id+'">'+data[2][i].name+' (SL yêu cầu:'+(data[1][i].quantity_sum)+')</option><select>';
            }
        }
        $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-3">'+select+'</div><div class="col-md-3 text-center" ><table width="100%">'+quantity_sum+'</table></div><div class="col-md-3 text-center" ><table width="100%">'+quantity_ok+'</table></div><div class="col-md-3 text-center" ><table width="100%">'+quantity_delivery+'</table></div><div>'+quantity_old+'</div></div>');
    }
    function getEdit(id,table){
        resetModal();
        $(".modal-title").text("Sửa thông tin");
    	$("#frm").attr("action","admin/"+table+"/postEdit/"+id);

    	$.ajax({
			url: 'admin/'+table+'/getEdit/'+id,
			type: 'get',
			success:function(data){
                console.log(data);
                if(table == "product"){
                    setDataClientProduct(data);
                }
                if(table == "category"){
                    setDataClientCategory(data);
                }
                if(table == "brand"){
                    setDataClientBrand(data);
                }
                if(table == "user"){
                    setDataClientUser(data);
                }
                if(table == "provider"){
                    setDataClientProvider(data);
                }
                if(table == "request"){
                    setDataClientRequest(data);
                }
                if(table == "import"){
                    setDataClientImport(data);
                }
                if(table == "export"){
                    setDataClientExport(data);
                }
			}
		});
    }
function confirmDel(id,table){
    var res = confirm("Bạn có muốn xóa dữ liệu này không?");
    if(res == true) {
        window.location.href = "admin/"+table+"/delete/"+id;
    }
}
function confirmActive(id,status){
    if(confirm("Xác nhận?")){
        window.location.href = "admin/slider/confirm/"+id+"/"+status;
    }
}
function resetModal(){
    $('#user_old').removeAttr('disabled');
    $("#address").removeAttr('readonly');
    $("#phone_reciever").removeAttr('readonly');
    $('#request_import').removeAttr('disabled');
    $("#order_status").addClass('hidden');
    $("#tracking").addClass('hidden');
    $("#tracking").val('');
    $("#address").val("");
    $('#request_import option:selected').removeAttr('selected');
    $("#them").removeClass('hidden');
    $("#btnAdd").removeClass('hidden');
    $('#listProduct').empty();
    $("#name").val("");
        $("#description").val("");
        $("#quantity").val("");
        $("#price_sell").val("");
        $("#price_import").val("");
        $('#brand').removeAttr('selected').find('option:first').attr('selected', 'selected');
        $('#category').removeAttr('selected').find('option:first').attr('selected', 'selected');
        $('#view_Img').attr({
            hidden: 'true'
        });
        $(".thum").remove();

}
$(document).ready(function() {
    $(".add_img").click(function(event) {
        var temp = makeClass();
        var className ="'"+temp+"'";
        var s = '<div class="col-md-2  thum '+temp+'"><label><img height="100%" src="frontend_asset/images/plus.png" width="100%"> <input type="file"  onchange="preview('+className+')" name="product_img[]" style="display: none;" accept="image/*"></label><span class="remove_img_preview" ></span>';
        $("#product_img").append(s);
    });


    $("#add-product-import").click(function() {
        var product_id = $("#product").val();
        var quantity = $("#quantity").val();
        var price_sell = $("#price_sell").val();
        var price_import = $("#price_import").val();
        var description = $("#description").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'admin/import/createSession',
            type: 'post',
            data: {'product_id':product_id,'quantity':quantity,'price_sell':price_sell,'price_import':price_import,'description':description},
            success:function(data){
                $("#product").val('');
                $("#quantity").val('');
                $("#price_sell").val('');
                $("#price_import").val('');
                $("#description").val('');
                $.notify("Thành công!","success");
            }
        });
    });
     $("#add-product-request").click(function() {
        var provider = $("#provider").val();
        var product_id = $("#product").val();
        var quantity = $("#quantity").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'admin/request/createSession',
            type: 'post',
            data: {'provider':provider,'product_id':product_id,'quantity':quantity},
            success:function(data){
                $("#product").val('');
                $("#quantity").val('');
                $.notify("Thành công!","success");
            }
        });
    });
    $("#add-product-export").click(function() {
        var product_id = $("#product").val();
        var quantity = $("#quantity").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'admin/export/createSession',
            type: 'post',
            data: {'product_id':product_id,'quantity':quantity},
            success:function(data){
                $("#product").val('');
                $("#quantity").val('');
                $.notify("Thành công!","success");
            }
        });
    });


    $("#product").change(function() {
        var url = "admin/import/getQty/"; 
        var id = $(this).parent().find('#product').val();
            $.ajax({
            url: url+id,
            type: 'GET',
            success:function(data){    
                console.log(data);
                $("#qtyMax").removeClass('hidden');
                $("#qtyMax").val(data);
                $("#qtyMax").html("Số lượng trong kho: "+data);
                }
        });
    });

    $("#quantity-export").change(function() {
        if($("#quantity").val() > $("#qtyMax").val()) {
            alert("Không đủ số lượng trong kho!");
        }
    });
});

//remove preview image
$("body").on("click",".remove_img_preview",function(){ 
      $(this).parents(".thum").remove();
});

//make random class
function makeClass() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

     return text;
}   
 
//preview image
function preview(className){    
    var reader = new FileReader();
    reader.onload = function () {
        $($.parseHTML('<img>')).attr('src', reader.result).attr('style',"top:-158px;position:relative;width:100%;height:100%;").appendTo(document.getElementsByClassName(className));
    }
    reader.readAsDataURL(event.target.files[0]);
}


function addList(){
    window.option ="";
    console.log(arrayIdProduct);
    var url = "admin/import/getListProduct"; 
            $.ajax({
            url: url,
            type: 'GET',
            success:function(data){    
                for(var i = 0; i< data.length; i++){
                    // console.log(arrayIdProduct, arrayIdProduct.indexOf(data[i].id.toString()), data[i].id);
                    if(arrayIdProduct.indexOf(data[i].id.toString()) < 0){
                        window.option += '<option value="'+data[i].id+'">'+data[i].name+'(Số lượng:'+data[i].quantity+')</option>';
                    }
                }
                $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-6"><select class="form-control arrayProduct" required="true" name="product_id[]" id="" class="getQty"><option value="">Chọn sản phẩm</option>'+option+'</select></div><div class="col-md-5"><input type="text" class="form-control" id="quantity" name="quantity[]" placeholder="Nhập số lượng" required="true" width="100%"></div><div class="col-md-1 del"><a href="javascript:void(0)" class="btn btn-danger" >-</a></div></div>');
            }
        });
    
}

//delete

$("body").on("click",".del",function(event){ 
        var temp = $(this).parent().find('.arrayProduct').val();
        var i = arrayIdProduct.indexOf(temp.toString());
        if (i != -1) {
            arrayIdProduct.splice(i,1);
        }
        $(this).parent().remove();
});

window.arrayIdProduct = new Array();
$("body").on("change",".arrayProduct",function(event){ 
    console.log($(this).val());
    arrayIdProduct.push($(this).val());
});


//nhập đơn nhập
$(document).ready(function() {
    $("#request_import").change(function() {
        var id =$(this).val();
        var select = ""; 
        var quantity_ok = "";
        var quantity_sum = "";
        var quantity_delivery = "";
        var url = "admin/import/getListProductRequest/"+id; 
            $.ajax({
            url: url,
            type: 'GET',
            success:function(data){    
                console.log(data);
                $("#provider").removeClass('hidden');
                $("#provider").html("Nhà cung cấp: "+data[3].name);
                $("#listProduct").empty();
                for(var i = 0; i< data[1].length; i++){
                    // console.log(arrayIdProduct, arrayIdProduct.indexOf(data[i].id.toString()), data[i].id);
                    if(data[1][i].product_id == data[2][i].id){
                        quantity_ok += '<input type="number" style="margin-top:5px;" class="form-control" id="quantity_ok" min="0" name="quantity_ok[]"   placeholder="SL đạt yêu cầu, max = '+data[1][i].quantity+' sản phẩm" max="'+data[1][i].quantity+'" width="100%">';
                        quantity_sum += '<input type="text" class="form-control hidden" id="quantity_sum" name="quantity_sum[]" value="'+data[1][i].quantity+'"  width="100%">';
                        quantity_delivery += '<input type="number" style="margin-top:5px;" class="form-control" id="quantity_delivery" name="quantity_delivery[]" required="true"  placeholder="Số lượng giao tới"  width="100%">';
                        select += '<select class="form-control arrayProduct" style="margin-top:5px;" name="product_id[]" id="" class="getQty"><option value="'+data[1][i].product_id+'">'+data[2][i].name+' (SL yêu cầu:'+data[1][i].quantity+')</option><select>';
                    }
                }
                $("#listProduct").append('<div class="row" style="padding-top:5px;"><div class="col-md-6">'+select+'</div><div class="col-md-3">'+quantity_delivery+'</div><div class="col-md-3">'+quantity_ok+'</div><div class="col-md-6">'+quantity_sum+'</div></div>');
            }
        });
    });
});


$("#type").change(function() {
    var type = $(this).val();
    window.location.href = "admin/report/getReport/"+type;
});