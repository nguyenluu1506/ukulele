<?php

Route::group(['middleware' => 'admin'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
// list all lfm routes here...
});
//backend
Route::prefix('admin')->group(function(){
	
  Route::get('/', 'Admin\AdminController@index')->name('admin');
  // Route::post('/postAdd', 'Admin\AdminController@postAdd')->name('admin.postAdd');
  Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::post('/logout','Auth\AdminLoginController@logout')->name('admin.logout');


  // for brand
	Route::prefix('/brand')->group(function(){
	  Route::get('/', 'Admin\BrandController@index')->name('admin.brand');
	  Route::post('/postAdd', 'Admin\BrandController@postAdd')->name('admin.brand.postAdd');
	  Route::get('/getEdit/{id}', 'Admin\BrandController@getEdit')->name('admin.brand.getEdit');
	  Route::post('/postEdit/{id}', 'Admin\BrandController@postEdit')->name('admin.brand.postEdit');
	  Route::get('/delete/{id}', 'Admin\BrandController@delete')->name('admin.brand.delete');
	});

	// for provider
	Route::prefix('/provider')->group(function(){
	  Route::get('/', 'Admin\ProviderController@index')->name('admin.provider');
	  Route::post('/postAdd', 'Admin\ProviderController@postAdd')->name('admin.provider.postAdd');
	  Route::get('/getEdit/{id}', 'Admin\ProviderController@getEdit')->name('admin.provider.getEdit');
	  Route::post('/postEdit/{id}', 'Admin\ProviderController@postEdit')->name('admin.provider.postEdit');
	  Route::get('/delete/{id}', 'Admin\ProviderController@delete')->name('admin.provider.delete');
	});
	//for category
	Route::prefix('/category')->group(function(){
	  Route::get('/', 'Admin\CategoryController@index')->name('admin.category');
	  Route::post('/postAdd', 'Admin\CategoryController@postAdd')->name('admin.category.postAdd');
	  Route::get('/getEdit/{id}', 'Admin\CategoryController@getEdit')->name('admin.category.getEdit');
	  Route::post('/postEdit/{id}', 'Admin\CategoryController@postEdit')->name('admin.category.postEdit');
	  Route::get('/delete/{id}', 'Admin\CategoryController@delete')->name('admin.category.delete');
	});

    //for product
    Route::prefix('/product')->group(function(){
        Route::get('/', 'Admin\ProductController@index')->name('admin.product');
        Route::post('/postAdd', 'Admin\ProductController@postAdd')->name('admin.product.postAdd');
        Route::get('/cfActive/{id}/{status}', 'Admin\ProductController@cfActive')->name('admin.product.cfActive');
        Route::get('/deleteImg/{id}', 'Admin\ProductController@deleteImg')->name('admin.product.deleteImg');
        Route::get('/getEdit/{id}', 'Admin\ProductController@getEdit')->name('admin.product.getEdit');
        Route::post('/postEdit/{id}', 'Admin\ProductController@postEdit')->name('admin.product.postEdit');
        Route::get('/delete/{id}', 'Admin\ProductController@delete')->name('admin.product.delete');
    });

	//for slider
	Route::prefix('/slider')->group(function(){
	  Route::get('/', 'Admin\SliderController@index')->name('admin.slider');
	  Route::post('/postAdd', 'Admin\SliderController@postAdd')->name('admin.slider.postAdd');
	  Route::get('/confirm/{id}/{status}', 'Admin\SliderController@confirm')->name('admin.slider.confirm');
	  //Route::get('/delete/{id}', 'Admin\SliderController@delete')->name('admin.slider.delete');
	});
	//for user
	Route::prefix('/user')->group(function(){
	  Route::get('/', 'Admin\UserController@index')->name('admin.user');
	  Route::get('/getEdit/{id}', 'Admin\UserController@getEdit')->name('admin.user.getEdit');
	  Route::post('/postEdit/{id}', 'Admin\UserController@postEdit')->name('admin.user.postEdit');
	  Route::get('/delete/{id}', 'Admin\UserController@delete')->name('admin.user.delete');
	});


	//for export-bill
	Route::prefix('/export')->group(function(){
 		Route::get('/', 'Admin\ExportController@index')->name('admin.export');
 		Route::get('/add', 'Admin\ExportController@showAddForm')->name('admin.export.add');
 		Route::get('/getEdit/{id}', 'Admin\ExportController@getEdit')->name('admin.export.getEdit');
 		Route::post('/postEdit/{id}', 'Admin\ExportController@postEdit')->name('admin.export.postEdit');
 		Route::get('/cf_ActiveExport/{id}', 'Admin\ExportController@cf_ActiveExport')->name('admin.export.cf_ActiveExport');
 		Route::get('/getProduct', 'Admin\ExportController@getProduct')->name('admin.export.getProduct');
 		Route::post('/createSession', 'Admin\ExportController@createSession')->name('admin.export.createSession');
 		Route::post('/postAdd', 'Admin\ExportController@postAdd')->name('admin.export.postAdd');
 		Route::post('/add', 'Admin\ExportController@postAdd')->name('admin.export.postadd');
 		Route::get('/getDetail/{id}', 'Admin\ExportController@getDetail')->name('admin.export.getDetail');
	});

	//for import-bill
	Route::prefix('/import')->group(function(){
	  Route::get('/', 'Admin\ImportController@index')->name('admin.import');
	  Route::get('/getEdit/{id}', 'Admin\ImportController@getEdit')->name('admin.import.getEdit');
	  Route::get('/getListProduct', 'Admin\ImportController@getListProduct')->name('admin.import.getListProduct');
	  Route::get('/getListProductRequest/{id}', 'Admin\ImportController@getListProductRequest')->name('admin.import.getListProductRequest');
	  Route::post('/postEdit/{id}', 'Admin\ImportController@postEdit')->name('admin.import.postEdit');
	  Route::post('/postAdd', 'Admin\ImportController@postAdd')->name('admin.import.postAdd');
	});

	//for request 
	Route::prefix('/request')->group(function(){
	  	Route::get('/', 'Admin\RequestController@index')->name('admin.request');
	  	Route::post('/postAdd', 'Admin\RequestController@postAdd')->name('admin.request.postAdd');
	  	// Route::get('/add', 'Admin\RequestController@showAddForm')->name('admin.request.add');
	  	Route::get('/getEdit/{id}', 'Admin\RequestController@getEdit')->name('admin.request.getEdit');
	  	Route::post('/postEdit/{id}', 'Admin\RequestController@postEdit')->name('admin.request.postEdit');
	});

	//for report
	Route::prefix('/report')->group(function(){
	  	Route::get('/', 'Admin\ReportController@index')->name('admin.report');
	  	Route::get('/postMonth', 'Admin\ReportController@postMonth')->name('admin.report.postMonth');
	  	Route::get('/getReport/{type}', 'Admin\ReportController@getReport')->name('admin.report.getReport');
	  	Route::get('/getReportProduct', 'Admin\ReportController@getReportProduct')->name('admin.report.getReportProduct');
	});
});


//front_end
	/*get view*/
	//home
	Route::get('/','Frontend\HomeController@index')->name('frontend.home');
	//user
	Route::get('/user', 'Frontend\UserController@index')->name('frontend.user');
	Route::post('/user', 'Frontend\UserController@postEdit')->name('frontend.user.postEdit');
	Route::get('/bill', 'Frontend\UserController@getBill')->name('frontend.user.getBill');
	Route::get('/bill-detail/{id}', 'Frontend\UserController@getBillDetail')->name('frontend.user.getBillDetail');
	Route::post('/rating', 'Frontend\UserController@postRating')->name('frontend.user.postRating');

	Route::get('/cancelOrder/{id}', 'Frontend\UserController@cancelOrder')->name('frontend.user.cancelOrder');

	//contact
	Route::get('/contact', 'Frontend\ContactController@index')->name('frontend.contact.getContact');
	Route::post('/postContact', 'Frontend\ContactController@postContact')->name('frontend.contact.postContact');
	
	//shop
	Route::get('/shop', 'Frontend\ShopController@index')->name('frontend.getShop');
	Route::get('/addCart/{id}', 'Frontend\ShopController@addCart')->name('frontend.addCart');
	Route::get('/detail/{id}', 'Frontend\ShopController@getProductDetail')->name('frontend.getProductDetail');
	Route::get('/cart', 'Frontend\ShopController@getCart')->name('frontend.getCart');
	Route::get('/category/{id}', 'Frontend\ShopController@getProductByCategoryId')->name('frontend.getProductByCategoryId');
	Route::get('/brand/{id}', 'Frontend\ShopController@getProductByBrandId')->name('frontend.getProductByBrandId');
	Route::get('/getQtyCart', 'Frontend\ShopController@getQtyCart')->name('frontend.getQtyCart');
	Route::get('/updateCartUp/{id}/{qty}', 'Frontend\ShopController@updateCartUp')->name('frontend.updateCartUp');

	Route::get('/updateCartInput/{id}/{qty}', 'Frontend\ShopController@updateCartInput')->name('frontend.updateCartInput');

	Route::get('/updateCartDown/{id}/{qty}', 'Frontend\ShopController@updateCartDown')->name('frontend.updateCartDown');
	Route::get('/delItemInCart/{id}', 'Frontend\ShopController@delItemInCart')->name('frontend.delItemInCart');
	//checkout
	Route::get('/checkout', 'Frontend\CheckoutController@index')->name('frontend.getCheckout');
	Route::get('/getDistrict/{id}', 'Frontend\CheckoutController@getDistrict')->name('frontend.getDistrict');
	Route::get('/getWard/{id}', 'Frontend\CheckoutController@getWard')->name('frontend.getWard');
	Route::post('/checkout', 'Frontend\CheckoutController@postCheckout')->name('frontend.postCheckout');





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

