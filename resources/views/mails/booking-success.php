<h2><center>Thông tin đặt hàng</center></h2>
<h3><center>Cửa hàng TNGUITAR</center></h3>
<center><b>Mã đơn yêu cầu: RQ-{{$id}}</b></center>
<table cellpadding="5px" align="center">
	<tr>
		<th>Danh sách SP</th>
		<th>Số lượng</th>
	</tr>
	@foreach($product as $pro)
	@foreach($quantity as $quan)
	<tr>
		<td>{{ $pro->name }}</td>
		<td>{{ $quan->quantity }}</td>
	</tr>
	@endforeach
	@endforeach
</table>
<center>Yêu cầu Nhà cung cấp phản hồi email này để xác nhận hàng hóa và thời gian giao hàng. Xin cảm ơn!</center>