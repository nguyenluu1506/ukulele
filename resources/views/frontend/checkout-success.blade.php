@extends('frontend.layouts.main')
@section('content')
<div class="container" style="margin-top: 30px;">
	<div class="row">
		<div class="checkout-left">
			<div class="address_form_agile">
				<h3><center style="color: red;">Thông báo đặt hàng thành công</center></h3>
				<center>Cảm ơn bạn đã mua sản phẩm của TNGUITAR. Hãy chờ một vài ngày để nhận được hàng nhé. Mọi thắc mắc xin liên hệ với chúng tôi qua email <span style="color: red;">support24h.tnguitar@gmail.com</span>. Hoặc hotline: 0123123123</center>
				<div class="col-md-6">
					<h4><center>Thông tin đơn hàng</center></h4>
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Sản phẩm</th>
								<th>Số lượng</th>
								<th>Giá</th>
							</tr>
						</thead>
						<tbody>
							@foreach($result->detail as $detail)
							<tr>
								<td>{{$detail->product->name}}</td>
								<td>{{$detail->quantity}}</td>
								<td>{{$detail->price_sell}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-6">
					<h4><center>Thông tin nhận hàng</center></h4>
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Họ tên</th>
								<th>SĐT</th>
								<th>Địa chỉ nhận hàng</th>
								<th>Thanh toán</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ $result->name_reciever }}</td>
								<td>{{ $result->phone_reciever }}</td>
								<td>{{ $result->address_reciever }}</td>
								<td>
									@if($result->payment == 'khi-nhan-hang')
									Thanh toán khi nhận hàng
									@endif
									@if($result->payment == 'chuyen-khoan')
									Thanh toán chuyển khoản trực tiếp
									@endif
									@if($result->payment == 'ngan-luong')
									Thanh toán qua Ngân lượng
									@endif
								</td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-primary" style="margin-bottom: 30px; float: right;" href="{{ route('frontend.home') }}">Tiếp tục mua hàng</a>
				</div>
			</div>
		</div>
	</div>
</div>
	
@endsection