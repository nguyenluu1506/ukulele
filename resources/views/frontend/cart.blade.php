@extends('frontend.layouts.main')
@section('content')
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Trang chủ</a></li>
				  <li class="active">Giỏ hàng</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				@if(count($cart) > 0)
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="description">Tên Sản phẩm</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total" style="width: 20%;">Thành tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<form method="POST" action="" name="cartList">
						
						@foreach($cart as $item)
						<tr id="item_{{$item->rowId}}" style="border-bottom:1px solid #fff; ">
							<td class="cart_product" style="margin-left: 0px;">
								<a href=""><img src="{{$item->options->img}}" width="120px" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="{{ route('frontend.getProductDetail',$item->id) }}">{{$item->name}}</a></h4>
							</td>
							<td class="cart_price">
								<!-- <input type="text" style="border: none; background:#ff000000; " readonly value="{{$item->price}}"> -->
								<p>{{number_format($item->price)}}<sup>đ</sup></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									
									<a href="javascript:void(0)" id="{{$item->rowId}}" class="cart_quantity_up" > + </a>
									<input id="{{$item->rowId}}" class="cart_quantity_input" type="text" name="{{$item->rowId}}" value="{{$item->qty}}" autocomplete="off" size="2"/>
									<a  class="cart_quantity_down"  id="{{$item->rowId}}" href="javascript:void(0)"> - </a>
									
								</div>
							</td>
							<td class="total">
								<input id="total_{{$item->rowId}}" type="text" readonly style="border: none; background:#ff000000; font-size: 20px;" value="{{number_format(($item->price*$item->qty))}}VNĐ" />
								
							</td>
							<td class="cart_delete" style="">
								<a class="cart_quantity_delete" id="{{$item->rowId}}" style="background-color: red; padding: 5px 12px;" href="javascript:void(0)"><i class="fa fa-times"></i></a>
							</td>
						</tr>

						@endforeach
						</form>
						<tr>
							<td><h3>Tổng tiền:</h3></td>
							<td></td>
							<td class="total"><input  id="subtotal" value="{{$total}}" type="text" style="border: 0px;width: 70%; font-size: 24px;  background: #ff000000;" readonly ></td>
						</tr>
						
					</tbody>
				</table>
				<a href="shop" style="padding:10px 20px; margin: 10px 0;" class="btn-warning pull-left"><span class="icon-arrow-left"></span> Tiếp tục mua hàng </a>
			
			<a href="{{ route('frontend.getCheckout')}}" style="padding:10px 20px; margin: 10px 0;" class="btn-warning pull-right">Đặt hàng <span class="icon-arrow-right"></span></a>
				@else
				<div style="text-align: center;">
					<img src="frontend_asset/images/empty.png" alt="" >
				</div>
				@endif
			</div>
			<div>
			
			
			
		</div>
		</div>
	</section> <!--/#cart_items-->
@endsection