<style type="text/css">
	.abc:hover{
		color: red !important;
	}
</style>
<div class="left-sidebar">
	<h2>Danh mục</h2>
	<div class="panel-group category-products" id="accordian"><!--category-products-->
		@foreach($category as $cate)
		<div class="panel panel-default abc" style="margin: 10px 0;">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="abc" href="{{ route('frontend.getProductByCategoryId',$cate->id) }}">
						{{$cate->name}}
					</a>
				</h4>
			</div>
		</div>
		@endforeach
	</div><!--/category-products-->

	<div class="brands_products">
		<h2>Thương hiệu</h2>
		<div class="brands-name">
			<ul class="nav nav-pills nav-stacked">
				@foreach($brand as $br)
				<li style="margin: 10px 0;"><a href="{{ route('frontend.getProductByBrandId',$br->id) }}"> <span class="pull-right"></span>{{$br->name}}</a></li>
				@endforeach
			</ul>
		</div>
	</div><!--/brands_products-->



	<div class="shipping text-center"><!--shipping-->
		<img src="public/images/home/shipping.jpg" alt="" />
	</div><!--/shipping-->

</div>