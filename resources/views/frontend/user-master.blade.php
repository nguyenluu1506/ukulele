@extends('frontend.layouts.main')
@section('content')
	<section style="margin-top: 50px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<style type="text/css">
					.a:hover{
						color: red !important;
						font-weight: bold;
					}
					.active{
						background: #d3c0c0;
						font-weight: bold;
					}
				</style>
				<div class="left-sidebar">
					<h2 style="margin-bottom: 10px;">Bảng điều khiển</h2>
					<ul class="list-group">
					  <li @if(isset($user)) class="active list-group-item " @endif class="list-group-item "><a href="{{ route('frontend.user') }}" class="a">Thông tin cá nhân</a></li>
					  <li @if(isset($bill)) class="active list-group-item " @endif class="list-group-item "><a href="{{ route('frontend.user.getBill') }}" class="a">Lịch sử đặt hàng</a></li>
					  <!-- <li @if(isset($inbox)) class="active list-group-item " @endif class="list-group-item "><a href="" class="a">Hộp thư</a></li> -->
					</ul>


				</div>
			</div>				
		<div class="col-sm-9 padding-right">
			@yield('content-user')	
		</div>
	</section>
@stop