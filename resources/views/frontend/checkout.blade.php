@extends('frontend.layouts.main')
@section('content')
<div class="container" style="margin-top: 30px;">
	<div class="row">
		<div class="checkout-left">
			<div class="address_form_agile">
				<div class="col-md-6">
					<h4><center>Thông tin người đặt</center></h4>
					<form action="" method="post" class="creditly-card-form agileinfo_form">
						<div class="creditly-wrapper wthree, w3_agileits_wrapper">
							<div class="information-wrapper">
								<div class="first-row">
									<div class="controls" style="margin: 10px 0;">
										<input class="billing-address-name form-control" type="text" id="name" placeholder="Họ và tên" value="{{ $user->name }}" disabled="true">
									<div class="controls" style="margin: 10px 0;">
										<input type="text" class="form-control" placeholder="Số điện thoại người nhận hàng" id="phone" value="{{ $user->phone }}" disabled="true">								
									</div>
									<div class="controls" style="margin: 10px 0;">
										<input type="text" class="form-control" placeholder="Địa chỉ cụ thể. VD: Số nhà..." id="address" value="{{ $user->address }}" disabled="true">
									</div>
									<div class="clear"> </div>
									<select  class="col-xs-4 col-md-4 form-control" disabled="true" id="province" style="margin: 5px 0;">
										<option value="{{ $user->Province->id }}">{{ $user->Province->name }}</option>
									</select>
									<select   class="col-xs-4 col-md-4 form-control" disabled="true" id="district" style="margin: 5px 0;">
										<option value="{{ $user->District->id }}">{{ $user->District->name }}</option>
									</select>
									<select  class="col-xs-4 col-md-4 form-control" disabled="true" id="ward" style="margin: 5px 0;">
										<option value="{{ $user->Ward->id }}">{{ $user->Ward->name }}</option>
									</select>
									<div class="clear"> </div>
								</div>
								<input type="checkbox" name="" id="check" style="width: 20px; height: 15px;">
								<label for="check">Thông tin nhận hàng và đặt hàng giống nhau</label>
								<br><br>
							</div>
						</div>
					</div>
					</form>
				</div>
				<div class="col-md-6">
					<h4><center>Thông tin giỏ hàng</center></h4>
					<table class="table table-condensed">
						<thead>
							<tr class="cart_menu">
								<td class="image">Sản phẩm</td>
								<td class="description">Tên Sản phẩm</td>
								<td class="price">Giá</td>
								<td class="quantity">Số lượng</td>
								<td class="total" style="width: 20%;">Tổng tiền</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							@foreach($cart as $item)
							<tr id="item_{{$item->rowId}}" style="border-bottom:1px solid #fff; ">
								<td class="cart_product" style="margin: 0px;">
									<a href=""><img src="{{$item->options->img}}" width="50px" alt=""></a>
								</td>
								<td class="cart_description">
									<p><a href="{{ route('frontend.getProductDetail',$item->id) }}">{{$item->name}}</a></p>
								</td>
								<td class="cart_price">
									<p>{{number_format($item->price)}}<sup>đ</sup></p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<input id="qty_{{$item->rowId}}" class="cart_quantity_input" type="text" name="qty_{{$item->rowId}}" value="{{$item->qty}}" autocomplete="off" size="1" readonly="true" disabled="true" />
									</div>
								</td>
								<td class="total">
									<input id="total_{{$item->rowId}}" type="text" readonly style="border: none; background:#ff000000;" value="{{number_format(($item->price*$item->qty))}}" />
									
								</td>
							</tr>
							@endforeach
							<tr>
								<td>Tổng tiền:</td>
								<td></td>
								<td></td>
								<td></td>
								<td class="total"><input  id="subtotal" value="{{$total}}" type="text" style="border: 0px;width: 70%;   background: #ff000000;" readonly ></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="checkout-left">
			<div class="address_form_agile">
				<div class="clear-fix"> </div>
				<div class="col-md-12" style="margin-bottom: 50px;">
					
					<form action="" method="post" class="creditly-card-form agileinfo_form" >
						{{ csrf_field() }}
						<div class="creditly-wrapper wthree, w3_agileits_wrapper" >
							<div class="information-wrapper col-md-6" style="padding:0px;">
								<h4><center>Thông tin nhận hàng</center></h4>
								<div class="first-row">
									<div class="controls" style="margin: 10px 0;">
										<input class="billing-address-name form-control" type="text" name="name_reciever" id="name_reciever" placeholder="Họ và tên" required="">
									</div>
									<div class="controls" style="margin: 10px 0;">
										<input type="text" class="form-control" placeholder="Số điện thoại người nhận hàng" name="phone_reciever" id="phone_reciever" required="">
									</div>										
									<div class="controls" style="margin: 10px 0;">
										<input type="text" class="form-control" placeholder="Địa chỉ cụ thể. VD: Số nhà..." name="address_reciever" id="address_reciever" required="">
									</div>
									<div class="clear"> </div>
									<select class="form-control col-xs-4 col-md-4"  name="province_reciever" id="province_reciever" style="margin: 5px 0;">
										<option>Chọn Tỉnh/Thành phố</option>
										@foreach($province as $pr)
										<option value="{{ $pr->id }}">{{ $pr->name }}</option>
										@endforeach
									</select>
									<select  class="form-control col-xs-4 col-md-4" name="district_reciever" id="district_reciever" style="margin: 5px 0;">
										<option>Chọn Quận/Huyện</option>
									</select>
									<select  class="form-control col-xs-4 col-md-4" name="ward_reciever" id="ward_reciever" style="margin: 5px 0;">
										<option>Chọn Phường/Xã</option>
									</select>
									<div class="clear"> </div>
										
								</div>

								
							</div>
							<div class="col-md-6">
								<h4><center>Thông tin thanh toán</center></h4>
								<div class="controls" style="margin: 10px 0;">
									<input class="" type="radio" name="payment_name" id="khi-nhan-hang" value="khi-nhan-hang" placeholder="" required="" data-toggle="collapse" data-target="#nhan-hang" checked="">
									<label for="khi-nhan-hang">Thanh toán khi nhận hàng</label> <br>
									<div id="nhan-hang" class="panel-collapse collapse in " area-explain="true">
							            <div class="panel-body">
							              <p>Vui lòng chuẩn bị sẵn số tiền in trên giá trị đơn hàng khi có bưu tá gọi nhận hàng và thanh toán trực tiếp với bưu tá</p>
							            </div>
						            </div>

									<input class="" type="radio" name="payment_name" id="chuyen-khoan" value="chuyen-khoan" placeholder="" required="" data-toggle="collapse" data-target="#chuyenkhoan">
									<label for="chuyen-khoan">Thanh toán chuyển khoản trực tiếp</label> <br>
									<div id="chuyenkhoan" class="panel-collapse collapse">
							            <div class="panel-body">
							              <p>Chuyển khoản trực tiếp tới Số tài khoản: 023243238413, ngân hàng Vietcombank, chi nhánh Hoàn Kiếm - Hà Nội. Nội dung chuyển khoản ghi rõ "họ tên - số điện thoại đặt hàng". Chúng tôi sẽ xác nhận thông tin thanh toán đơn hàng của bạn ngay sau đó </p>
							            </div>
						            </div>
									<input class="" type="radio" name="payment_name" id="ngan-luong" value="ngan-luong" placeholder="" required="" data-toggle="collapse" data-target="#nganluong">
									<label for="ngan-luong">Thanh toán với Ngân Lượng</label> <br>
								</div>
								
					            <div id="nganluong" class="panel-collapse collapse">
						            <div class="panel-body">
						              <p>Tới trang thanh toán trực tuyến uy tín <a href="">NganLuong.com</a></p>
						            </div>
					            </div>
							</div>
						</div>
						<input type="submit" class="submit check_out btn btn-success" style="float: right; margin-bottom: 10px;" value="Đặt hàng">
						
					</form>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
	
@endsection