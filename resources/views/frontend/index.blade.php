@extends('frontend.layouts.main')
@section('content')
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<?php $i=0; ?>
							  	@foreach($slider as $sl)
							  	<?php $i++; ?>
							  		@if ($i==1) 
										<div class="item active">
									@else <div class="item">
									@endif
								<div class="col-sm-6">
									<h1><span>TNGUITAR</span>-SHOP</h1>
									<h2>{{ $sl->title }}</h2>
									<p>Chất lượng đảm bảo, giá cả hợp lý, hài lòng</p>
									<button type="button" class="btn btn-default get">Xem sản phẩm của chúng tôi</button>
								</div>
								<div class="col-sm-6">
									<img src="{{$sl->url}}" width="100%" class="img-responsive" alt="" />
								</div>
							</div>
							@endforeach
						</div>
					</div>
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row" >
				<div class="col-sm-3">
					@include('frontend.left')
				</div>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center" style="padding-top:3px;">Sản phẩm nổi bật</h2>

						@foreach($product as $pr)

						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{ $pr->avatarProduct->url ? $pr->avatarProduct->url : null }}" alt="" style="height: 350px !important;" />

											<h2 id="price">{{ number_format($pr->price_sell)}}<sup>đ</sup></h2>
											<p id="name">{{$pr->name}}</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</a>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<a href="{{ route('frontend.getProductDetail',$pr->id) }}" class="btn btn-default add-to-cart">Xem</a><br>
												<h2>{{ number_format($pr->price_sell)}}<sup>đ</sup></h2>
												<p>{{$pr->name}}</p>
												<a  onclick="addCart({{$pr->id}})"  href="javascript:void(0)" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</a>
											
												
											</div>
										</div>
								</div>
								
							</div>
						</div>
						@endforeach
						
					</div><!--features_items-->
					<div class="row">
							{{$product->links()}}
					</div>
					<div class="category-tab">
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<?php $i=0; ?>
								@foreach($category as $cate)
								<?php if($i == 4) break; ?>			
								<?php $i++; ?>
								@if($i==1)
								<li class="active">
								@else <li>
								@endif
										<a name="category_tab" id="" href="#{{$cate->id}}" data-toggle="tab">{{$cate->name}}</a>
									</li>
								
								@endforeach
							</ul>
						</div>
						<div class="tab-content">
							<?php $i=0; ?>
							@foreach($category as $cat)

							<?php $i++; ?>
								@if($i==1)
								<div class="tab-pane fade active in" id="{{$cat->id}}" >
								@else <div class="tab-pane fade in" id="{{$cat->id}}" >
								@endif
								<?php $j=0; ?>
								@foreach($cat->Product as $val)
								
								<?php if($j == 4) break; ?>	
								<?php $j++; ?>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{ $val->avatarProduct->url ? $val->avatarProduct->url : null  }}" style="height: 290px !important;" alt="" />
												<h2>{{ number_format($val->price_sell)  }} <sup>đ</sup></h2>
												<p>{{ $val->name }}</p>
												<a href="javascript:void(0)"  onclick="addCart({{ $val->id}})" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</a>
											</div>
											
										</div>
									</div>
								</div>
								@endforeach
							</div>
							@endforeach
						</div>
						
					</div>
					
				</div>

			</div>
		</div>
	</section>
@stop

	
	
	
	