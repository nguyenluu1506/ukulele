<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="contactinfo">
              <ul class="nav nav-pills">
                <li><a href="#"><i class="fa fa-phone"></i> 01656465883</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> nluu246@gmail.com</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="social-icons pull-right">
              <ul class="nav navbar-nav">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div><!--/header_top-->
    
    <div class="header-middle"><!--header-middle-->
      <div class="container">
        <div class="row" style="padding:5px;">
          <div class="col-sm-3">
            <div class="logo pull-left">
              <a href="{{ route('frontend.home') }}"><img src="images/logo.png" width="100%" alt="" /></a>
            </div>
            
          </div>
          <div class="col-sm-9" style="padding-top: 50px; padding-left: 150px;">
            <div class="mainmenu pull-left" >
              <ul class="nav navbar-nav collapse navbar-collapse" >
                <li><a href="{{ route('frontend.home') }}" class="active"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li ><a href="{{ route('frontend.getShop') }}"><i class="fa fa-shopping-bag" ></i> Cửa hàng</i></a>
                                </li> 
                <li><a href="{{ route('frontend.contact.getContact') }}">Liên hệ</a></li>
              </ul>
              <ul class="nav navbar-nav">
                <li><a href="{{ route('frontend.getCart') }}" style="margin: 0px;"><i class="fa fa-shopping-cart"></i> Giỏ hàng<span class="label label-warning" id="number-item" style="position: absolute;right: -14px;text-align: center;font-size: 9px;padding: 4px 5px;line-height: .9;">10</span></a></li>
                @guest
                <li class="dropdown">
                  <a href="" style="margin: 0px;""><i class="fa fa-user"></i> Tài khoản <i class="fa fa-angle-down"></i></a>
                  <ul role="menu" class="sub-menu">
                    <li>
                      <a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Đăng nhập</a>
                    </li>
                    <li>
                      <a href="{{ route('register') }}"><i class="fa fa-plus" ></i> Đăng ký</a>
                    </li>
                  </ul>
                </li>
                @else
                <li class="dropdown">
                  <a href="#" style="margin: 0px;""><i class="fa fa-user"></i> {{ Auth::user()->name }} <i class="fa fa-angle-down"></i></a>
                  <ul role="menu" class="sub-menu">
                    <li>
                                    
                      <a href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="fa fa-sign-in"></i> Đăng xuất</a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                    </li>
                    <li>
                      <a href="{{ route('frontend.user') }}"><i class="fa fa-info-circle" aria-hidden="true"></i> Thông tin tài khoản</a>
                    </li>
                  </ul>
                </li>
                @endguest
              </ul>
              <!-- <div class="search_box pull-right">
                <input type="text" class="form-control" placeholder="Tìm kiếm"/>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div><!--/header-middle-->
  </header><!--/header-->
