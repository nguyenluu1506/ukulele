<footer id="footer" style="background-color: #f0f0e9; " ><!--Footer-->
  <div class="footer-top">
    <div class="container" style="padding: 10px; border: none;">
      <div class="row">
        <div class="col-sm-6">
          <div class="companyinfo" style="color: #666663; margin: 0px;">
            <img src="images/logo.png" width="200px" alt="">
            <h4>Điện thoại: 01656465883</h4>
            <h4>Địa chỉ: Hoàng Mai, Hà Nội</h4>
          </div>
        </div>
        <div class="col-sm-6" style="text-align: center;">
          <div class="companyinfo">

            <h4><a href="#" style="color: #666663;">Hướng dẫn mua hàng</a></h4>
            <h4><a href="#" style="color: #666663;">Hình thức thanh toán</a></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer><!--/Footer-->
