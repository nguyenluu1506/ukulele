<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="{{asset('')}}">
    <title>Shop đàn</title>
    <link href="frontend_asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="frontend_asset/css/font-awesome.min.css" rel="stylesheet"><!-- 
    <link href="frontend_asset/css/prettyPhoto.css" rel="stylesheet">
    <link href="frontend_asset/css/price-range.css" rel="stylesheet"> -->
    <link href="frontend_asset/css/animate.css" rel="stylesheet">
    <link href="frontend_asset/css/main.css" rel="stylesheet">
    <link href="frontend_asset/css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="frontend_asset/images/ico/favicon.ico">
</head><!--/head-->

<body style="background-color: #fff0f0;">
  @include('frontend.layouts.header')
	@yield('content')
  @include('frontend.layouts.footer')
    
    <script src="frontend_asset/js/angular.min.js"></script>
    <script src="frontend_asset/app.js"></script>
    <script src="frontend_asset/js/jquery.js"></script>
    <script src="frontend_asset/js/cart.js"></script>
    <script src="frontend_asset/js/page.js"></script>
    <script src="frontend_asset/js/bootstrap.min.js"></script>
    <script src="frontend_asset/js/jquery.scrollUp.min.js"></script>
    <script src="frontend_asset/js/price-range.js"></script>
    <script src="frontend_asset/js/jquery.prettyPhoto.js"></script>
    <script src="frontend_asset/js/main.js"></script>
    <script src="js/notify.min.js"></script>
    <script>
        var a = $("#subtotal").val();
        $("#subtotal").val( a.substring(0,a.length-3)+" VNĐ");
    </script>

    <script>
        $(document).ready(function() {
            $("#hover").hover(function() {
                $(".tooltext").css('visibility', 'visible');
            }, function() {
                $(".tooltext").css('visibility', 'hidden');
            });
        });
        
    </script>
</body>
</html>
