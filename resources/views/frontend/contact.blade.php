@extends('frontend.layouts.main')
@section('content')
	
	 <div id="contact-page" class="container" style="margin-top:100px;">
    	<div class="bg">  	
    		<div class="row"> 
    			@if(!empty($success))
			  <div class="alert alert-success"> {{ $success }}</div>
			@endif 	
	    		<div class="col-sm-8">
	    				<h2 class="title text-center">Gửi ý kiến</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form"  action="{{ route('frontend.contact.postContact') }}" method="post">
				    		{{ csrf_field() }}
				            <div class="form-group col-md-6">
				                <input type="text" name="name" id="name" class="form-control" required="required" placeholder="Họ và tên" value="{{ Auth::user() ? Auth::user()->name : ''}}">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" id="email" class="form-control" required="required" placeholder="Email" value="{{ Auth::user() ? Auth::user()->email : ''}}">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" id="subject" class="form-control" required="required" placeholder="Chủ đề">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Nội dung"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" style="padding: 10px 30px;" value="Gửi">
				            </div>
				        </form>
	    		</div>
	    		<div class="col-sm-4">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Thông tin liên hệ</h2>
	    				<address>
	    					<p>TNGUITAR-SHOP.COM</p>
							<p>Hoàng Mai, Hà Nội</p>
							<p>Điện thoại: 0356465883</p>
							<p>Email: nluu246@gmail.com</p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Liên kết</h2>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-youtube"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div>
@stop