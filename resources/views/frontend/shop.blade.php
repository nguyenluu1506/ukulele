@extends('frontend.layouts.main')
@section('content')
	<section style="margin-top: 100px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					@include('frontend.left')
				</div>				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Sản phẩm</h2>
						@foreach($product as $pr)
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="{{ $pr->avatarProduct->url }}" alt="" style="height: 350px !important;" />
										<h2>{{number_format($pr->price_sell)}}</h2>
										<p>{{$pr->name}}</p>
										<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<a href="detail/{{$pr->id}}" class="btn btn-default add-to-cart">Xem</a><br>
											<h2>{{number_format($pr->price_sell)}}</h2>
											<p>{{$pr->name}}</p>
											<a onclick="addCart({{$pr->id}})"  href="javascript:void(0)" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</a>
										</div>
									</div>
								</div>
							</div>
					</div><!--features_items-->
					@endforeach

				</div>
				@if(isset($a))
				<div class="row">{{$product->links()}}</div>
				@endif
			</div>
		</div>
	</section>
@stop