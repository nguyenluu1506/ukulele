@extends('frontend.layouts.main')
@section('content')
<section>
	<div class="container" style="margin-top: 30px;">
		<div class="row">
			<div class="col-sm-3">
				@include('frontend.left')
			</div>				
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Thông tin sản phẩm</h2>
					<div class="row">
						<div class="col-sm-4">
							<div class="view-product">
								<img src="{{$product->avatarProduct->url}}" alt="" />
							</div>
							<div id="similar-product" class="carousel slide" data-ride="carousel">
								
							  <!-- Wrapper for slides -->
							    <div class="carousel-inner">
							    	<div class="item active" style="text-align: center;">
									  <a href=""><img src="{{$product->avatarProduct->url}}" width="100px" height="120px" alt=""></a>
									</div>
							    	@foreach($product->imageProduct as $img)
									<div class="item" style="text-align: center;">
									  <a href=""><img src="{{ $img->url }}" width="100px" height="120px" alt=""></a>
									</div>
									@endforeach
								</div>

							  <!-- Controls -->
							  <a class="left item-control" href="#similar-product" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right item-control" href="#similar-product" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>
						</div>

					</div>
					
					<div class="col-md-7" style="padding-left: 80px;">
						<h3>{{$product->name}}</h3>
						<div class="rating">
						   <?php  
	                            for($i = 1; $i<= $product->rating; $i++){
	                            ?>
	                                <i name="" class="fa fa-star" style="color: orange;"></i>
	                                <?php
	                            }
	                            for($i = 1; $i<= (5- $product->rating); $i++){
	                            ?>
	                                <i name="" class="fa fa-star-o"></i>
	                                <?php
	                            }  

	                        ?>
	                        <span>({{ $product->count_rating}} lượt đánh giá)</span>
						</div>
						
						<hr class="soft"/>

						
							<div class="control-group" style="line-height: 3;">
								<label class="control-label"><span>Giá: {{ number_format($product->price_sell)}} <sup> VNĐ</sup></span></label>
							</div>

							
							<h4>Hàng mới về</h4>
							<p>Tặng phần quà đặc biệt khi đến cửa hàng Check-in</p>
									<a style="padding: 10px 20px; background-color: #F5F597;" onclick="addCart({{ $product->id}})"  href="javascript:void(0)" type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a>
							<a href=""><img src="frontend_asset/images/share.png" class="share img-responsive" alt=""></a>
							</div>
							<hr />
							<div id="myTabContent" class="tab-content tabWrapper">
								<div class="tab-pane fade active in" id="home" style="padding-left: 20px; margin-top: 600px;">
									<h4 style="color: orange;">Chi tiết sản phẩm</h4>
									<table class="table table-striped">
										<tbody>
											
											<tr class="techSpecRow"><td class="techSpecTD1"><b>Loại:</b> {{$product->category->name}}</td><td class="techSpecTD2"></td></tr>
											<tr class="techSpecRow"><td class="techSpecTD1"><b>Thương hiệu:</b>  {{$product->brand->name}}</td><td class="techSpecTD2"> </td></tr>
											<tr class="techSpecRow"><td class="techSpecTD1"><b>Thông tin thêm:</b>  {!!$product->description!!}</td><td class="techSpecTD2"> </td></tr>
										</tbody>
									</table>
									<p></p>

								</div>
							</div>
							<div class="rating">
								<div class="tab-pane fade active in" id="home" style="padding-left: 20px;">
									<h4 style="color: orange">Đánh giá sản phẩm</h4><i>({{ $product->count_rating}} lượt đánh giá)</i>
									
											@foreach($rating as $value)
											<div class="" style="border-top: 1px solid grey;">
												<h5>{{ $value->user->name }}</h5>
												<?php 
						                            for($i = 1; $i<= $value->rating; $i++){
						                            ?>
						                                <i name="" class="fa fa-star" style="color: orange;"></i>

						                                <?php
						                            }
						                            for($i = 1; $i<= (5- $value->rating); $i++){
						                            ?>
						                                <i name="" class="fa fa-star-o"></i>
						                                <?php
						                            }  
						                        ?>
												<h6>{{ $value->comment }}</h6>
											</div>
												
											@endforeach

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
@endsection