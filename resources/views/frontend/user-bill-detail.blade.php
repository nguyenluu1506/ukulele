@extends('frontend.user-master')
@section('content-user')
<style>
	.fa-star{
    color: orange !important;
}
</style>
	<h2 class="title text-center">Chi tiết đặt hàng</h2>
	<table class="table table-condensed table-hover">
		<thead>
			<tr >
				<th>Sản phẩm</th>
				<th>Số lượng</th>
				<th>Giá</th>
				<th>Đánh giá sản phẩm<i style="display: block; color: red;  font-size: 12px;">(Bạn không thể đánh giá khi đơn hàng chưa thành công!)</i></th>
				<th>Khiếu nại trả hàng <br> <i style="color: red; display: block; font-size: 12px;">(Chỉ có hiệu lực trong vòng 7 ngày sau khi đơn hàng thành công!)</i></th>
			</tr>
		</thead>
		<tbody>
			@foreach($bill as $item)
			<tr>
				<td>{{ $item->product->name }}</td>
				<td>{{ $item->quantity }}</td>
				<td>
					{{ number_format($item->price_sell) }} VNĐ
				</td>
				<td>
					@if($item->is_rating == 0 )
					@if($order->status ==3)
						<a class="btn btn-xs btn-success" href="" data-toggle="modal" data-target="#modalForm" onclick="setVal({{$item->id}},{{$item->product_id}})">Đánh giá ngay</a>
					@else
						<a class="btn btn-xs btn-success" disabled href="" data-toggle="modal"  data-target="#modalForm" onclick="setVal({{$item->id}},{{$item->product_id}})">Đánh giá ngay</a>
						
					@endif
					@else
						<?php 
                            for($i = 1; $i<= $item->rating->rating; $i++){
                            ?>
                                <i name="" class="fa fa-star" style="color: orange;"></i>

                                <?php
                            }
                            for($i = 1; $i<= (5- $item->rating->rating); $i++){
                            ?>
                                <i name="" class="fa fa-star-o"></i>
                                <?php
                            }  
                        ?>
                        <p>{{ $item->rating->comment }}</p>
					@endif
				</td>
				<td>
					@if(\Carbon\Carbon::now()->diffInDays($item->created_at) < 7 && $order->status == 3)
					<a href="" class="btn btn-xs btn-info">Gửi khiếu nại</a>
					@else
					<a href="" class="btn btn-xs btn-default disabled">Gửi khiếu nại</a>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a class="btn btn-primary" href="{{ url()->previous() }}" style="margin-bottom: 20px;">Quay lại</a>

	<!-- Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalForm" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Đánh giá</h4>
				</div>
				<div class="modal-body">
					<form id="formRating" action="{{ route('frontend.user.postRating')}}" class="form-horizontal" method="post">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="col-sm-3 control-label" style="padding:0px !important;">Mức độ hài lòng <span style="color: red;">*: </span></label>
							<div class="col-sm-9">
								<div class="rating-div" >
									<i name="star1" class="fa fa-star-o" onclick="rating(1)"></i>
									<i name="star2" class="fa fa-star-o" onclick="rating(2)"></i>
									<i name="star3" class="fa fa-star-o" onclick="rating(3)"></i>
									<i name="star4" class="fa fa-star-o" onclick="rating(4)"></i>
									<i name="star5" class="fa fa-star-o" onclick="rating(5)"></i>
								</div>
								<!-- <p id="alert" style="color: red"></p> -->
								
							</div>
							<div class="row">
								<input class="col-md-1" type="number" id="star" name="star" value="" style="display: none;">
								<input class="col-md-1" type="number" id="detail_id" value="" name="detail_id" style="display: none;">
								<input class="col-md-1" type="number" id="product_id" value="" name="product_id" style="display: none;">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" style="padding:0px !important;">Ý kiến đánh giá <span style="color: red;">*: </span></label>
							<div class="col-sm-9">
								<textarea class="form-control " rows="5" name="comment" maxlength="255" required></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<button id="submit" type="submit" class="btn btn-primary pull-right" disabled="">Gửi đánh giá</button>
							</div>
						</div>

					</form>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
@stop