@extends('frontend.user-master')
@section('content-user')
	<h2 class="title text-center">Lịch sử đặt hàng</h2>
	<table class="table table-condensed table-hover">
		<thead>
			<tr >
				<th>Đơn hàng</th>
				<th>Thời gian đặt</th>
				<th>Tổng tiền</th>
				<th>Tình trạng</th>
				<th>Địa chỉ giao hàng</th>
				<!-- <th>Mã giảm giá</th> -->
				<th>Hành động</th>

			</tr>
		</thead>
		<tbody>
			@foreach($bill as $item)
			<tr>
				<td>EP-{{ $item->id}}</td>
				<td>{{ $item->date_order }}</td>
				<td>
					<?php
						$detail = $item->detail;
						$sum = 0;
						for($i=0; $i<sizeof($detail); $i++){
							$sum += $detail[$i]->quantity * $detail[$i]->price_sell;
						}
					?>
					{{ number_format($sum) }} VNĐ
				</td>
				<td>
					@if($item->status == 0)
					Đang chờ xác nhận
					@endif
					@if($item->status == 1)
					Đặt hàng thành công
					@endif
					@if($item->status == 2)
					Thanh toán thành công
					@endif
					@if($item->status == 3)
					Giao hàng thành công
					@endif
					@if($item->status == 4)
					<span style="color: red;">Đơn hàng đã hủy</span>
					@endif
				</td>
				<td>{{ $item->address_reciever }}</td>
				<td><a class="btn btn-info btn-xs" href="{{ route('frontend.user.getBillDetail',$item->id) }}">Xem chi tiết</a>
					@if($item->status == 0)
					  	<a class="btn-danger btn btn-xs" id="hover" href="{{ route('frontend.user.cancelOrder',$item->id)}}">Hủy đơn hàng</a>
					  	<i class="tooltext" style="color: red; visibility: hidden; display: block; font-size: 12px;">(Lưu ý: Bạn sẽ không thể hủy đơn hàng sau khi đã được xác nhận)</i>
					</div>
					@endif
					@if($item->status != 4 && $item->status != 0)
					  	<a class="btn-danger btn btn-xs" id="hover" href="" disabled>Hủy đơn hàng</a>
					  	<i class="tooltext" style="color: red; visibility: visible; display: block; font-size: 12px;">(Lưu ý: Bạn sẽ không thể hủy đơn hàng sau khi đã được xác nhận)</i>
					</div>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
@stop