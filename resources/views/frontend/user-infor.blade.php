@extends('frontend.user-master')
@section('content-user')
	
	<h2 class="title text-center">Thông tin cá nhân</h2>
	@if (session('success'))
	<span class="alert alert-success" role="alert">
	    <strong> <i class="fa fa-check" aria-hidden="true"></i> {{ session('success') }}</strong>
	</span>
	@endif
	<form action="" method="post" class="creditly-card-form agileinfo_form" style="margin-top: 30px;">
		{{ csrf_field() }}
		<div class="creditly-wrapper wthree, w3_agileits_wrapper" style="margin-bottom: 50px;">
			<div class="information-wrapper">
				<div class="first-row">
					<div class="controls" style="margin: 10px 0;">
						<input class="billing-address-name form-control" name="name" type="text" id="name" placeholder="Họ và tên" value="{{ $user->name }}" required>
					</div>
					<div class="controls" style="margin: 10px 0;">
						<input type="text" class="form-control" name="phone" placeholder="Số điện thoại người nhận hàng" id="phone" value="{{ $user->phone }}" required>								
					</div>
					<div class="controls" style="margin: 10px 0;">
						<input type="text" class="form-control" name="address" placeholder="Địa chỉ cụ thể. VD: Số nhà..." id="address" value="{{ $user->address ? $user->address : '' }}" required="true">
					</div>
					<div class="clear"> </div>
					<select  class="col-xs-4 col-md-4 form-control" name="province" required id="province_reciever" style="margin: 10px 0;">
						@if(isset($user->Province))
						<option value="{{ $user->Province->id }}" selected="selected">{{ $user->Province->name }}</option>
						@foreach($province as $pr)
						<option value="{{ $pr->id }}">{{ $pr->name }}</option>
						@endforeach
						@else
						<option value="">Chọn Tỉnh/Thành phố</option>
						@foreach($province as $pr)
						<option value="{{ $pr->id }}">{{ $pr->name }}</option>
						@endforeach
						@endif
					</select>
					<select   class="col-xs-4 col-md-4 form-control" name="district" required id="district_reciever" style="margin: 10px 0;">
						@if(isset($user->District))
						<option value="{{ $user->District->id }}">{{ $user->District->name }}</option>
						@else
							<option value="">Chọn Quận/Huyện</option>
						@endif
					</select>
					<select  class="col-xs-4 col-md-4 form-control" name="ward" required id="ward_reciever" style="margin: 10px 0;">
						@if(isset($user->Ward))
						<option value="{{ $user->Ward->id }}">{{ $user->Ward->name }}</option>
						@else
						<option value="">Chọn Phường/Xã</option>
						@endif
					</select>
				</div>
				<br><br>
				<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
			</div>
		</div>
	</form>
@stop