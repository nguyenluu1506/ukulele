@extends('admin.layouts.main')
@section('content')
<div class="comtainer" style="margin-left: 20px;">
    <div class="row"  >
        <h2 >Tổng quan</h2>
        <div class="col-lg-3 ">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{count($export)}}</h3>
                    <p>Đơn hàng chờ xác nhận</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{route('admin.export')}}" class="small-box-footer">
                  Xem tất cả <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 ">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{count($export_ok)}}</h3>
                    <p>Đơn hàng thành công</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{route('admin.export')}}" class="small-box-footer">
                  Xem tất cả <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
