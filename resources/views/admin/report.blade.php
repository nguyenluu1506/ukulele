@extends('admin.layouts.main')
@section('content')
<style>
	b.month-picker {
    position:relative; /*this is the key*/
    z-index:24; background-color:#ccc;
    color:#000;
    text-decoration:none
}

input.month-picker  {
    width:6em;
    text-align:center;
}

input.month-picker + span {
	display: none
}

input.month-picker + span button {
	text-align:center;
	width: 4em; 
	height: 3em;
}
input.month-picker + span select {
	margin:.5em;
}

input.month-picker.focus + span, 
input.month-picker:focus + span, 
input.month-picker + span:hover{ /*the span will display just on :hover state*/
    display:block;
    position:absolute;
    width:12em;
    top:2em;
    left:-.8em;
    background-color:#ddd;
    text-align: center;
	padding:6px;
	border-radius: 5px;
    box-shadow: 0 0 0 5px rgba(255, 255, 255, 0.4), 0 4px 20px rgba(0, 0, 0, 0.33);
}

b.month-picker label {
    font-family: Arial,Helvetica,sans-serif;
    font-size: .9em;
	float:left;
}
input[type=radio] {
    opacity:0;
	margin:0 -18px 0 0;
}

input[type=radio]+span {
 font: 13px/20px 'Lucida Grande',Tahoma,Verdana,sans-serif;
    -moz-box-sizing: content-box;
    background: linear-gradient(to bottom, #F1F1F1, #DFDFDF 70%, #DADADA) repeat scroll 0 0 #DFDFDF;
    border-color: #CECECE #BABABA #A8A8A8;

    border-style: solid;
    color: #555555;
    display: inline-block;
    font-size: 11px;
    font-weight: bold;
    height: 28px;
    line-height: 28px;
    outline: 0 none;
    text-decoration: none;
    text-shadow: 0 1px #FFFFFF;
    text-transform: capitalize;
	width:4em;
	
/**/
    border-radius: 3px;
    border-width: 0;
    box-shadow: 0 0 3px rgba(0, 0, 0, 0.3) inset, 0 1px 1px rgba(0, 0, 0, 0.4) inset, 0 1px #FFFFFF;
	margin: 2px 6px;
    padding: 1px 13px 0;
    vertical-align: -5px;
}

input[type=radio]:checked+span {
    background: none repeat scroll 0 0 #DFDFDF;
    border-top-color: #C9C9C9;

    border-radius: 3px 3px 2px 2px;
    border-width: 1px 1px 0;
    box-shadow: 0 1px #FDFDFD inset, 0 0 0 1px #EAEAEA inset, 0 1px #A8A8A8, 0 3px #BBBBBB, 0 4px #A8A8A8, 0 5px 2px rgba(0, 0, 0, 0.25);
    margin:  -2px 6px 6px 6px;
    padding: 0 12px;
    vertical-align: baseline;
    color:#06A35F;
}
</style>
	<div class="row">
	    <div class="col-xs-12">
			<div class="box print_hidden">
			    <div class="box-header">
				<h3 class="box-title">Thống kê</h3>
				<div class="form-group" style="">
					<select name="type" id="type" class="form-control">
					<option value="">Chọn tiêu chí thống kê</option>
					<option value="4">Thống kê đơn hàng hủy</option>
					<option value="5">Thống kê sản phẩm còn/sắp hết</option>
					<option value="6">Thống kê 10 sản phẩm đang được mua nhiều nhất</option>
				</select>
				</div>
				<div class="form-horizontal">
					<form action="{{ route('admin.report.postMonth') }}">
						Chọn tháng
					    <input id='mois' type="text" style="padding:5px;" />
					    <input type="submit" value="Gửi" class="btn btn-success" >
					</form>
				</div>
				</div>
		    </div>
			    <!-- /.box-header -->
			    <div class="box-body table-responsive no-padding">
				
				<table class="table table-hover table-bordered text-center">
					@if(!isset($status))
					@if(!isset($month) )
					<b>Tháng <span style="color: red;"><?php echo date('m'); ?></span> này có: <span style="color: red;">{{ count($data) }} đơn hàng đã được đặt</span> </b>
					@endif
					@if(isset($month))
						<b>Tháng <span style="color: red;">{{ $month }}</span> có: <span style="color: red;">{{ count($data) }} đơn hàng đã được đặt</span> </b>
					@endif
				    <tr>
						<th>Mã đơn hàng</th>
						<th>Tổng số sản phẩm bán</th>
						<th>Tổng số tiền nhập</th>
						<th>Tổng số tiền bán</th>
						<th>Lợi nhuận</th>
				    </tr>
					
				    <?php $sum = 0; ?>
				    @foreach($data as $item)
					    <?php $sum_quantity = 0; $sum_sell = 0; $sum_import=0; 
					    foreach ($item->detail as $value) {
							$sum_quantity += $value->quantity;
							$sum_import += $value->quantity*$value->price_import;
							$sum_sell += $value->quantity*$value->price_sell;
							$sum += ($value->quantity*$value->price_sell)-($value->quantity*$value->price_import);
						}
							?>
					    <tr>
							<td>EP-{{ $item->id }}</td>
							<td>{{ $sum_quantity }}</td>
							<td>{{ number_format($sum_import,0) }} VNĐ</td>
							<td>{{ number_format($sum_sell,0 )}} VNĐ</td>
							<th>{{ number_format($sum_sell - $sum_import,0 )}} VNĐ</th>
					    </tr>
				    @endforeach
				    <tr>
				    	<td></td>
				    	<th style="color: red;">Tổng lợi nhận:</th>
				    	<th style="color: red;">{{ number_format($sum,0) }} VNĐ</th>
				    	<th></th>
				    	<th></th>
				    </tr>
				@endif
				@if(isset($status) && $status ==5)
				<center style="color: red;">Danh sách sản phẩm đã hết hàng hoặc sắp hết</center>
					<tr>
						<th>Mã sản phẩm</th>
						<th>Tên sản phẩm</th>
						<th>Số lượng còn</th>
				    </tr>
				    @foreach($data as $val)
					<tr>
						<td>P-{{ $val->id}}</td>
						<td>{{ $val->name}}</td>
						<td>{{ $val->quantity}}</td>
					</tr>
				    @endforeach
				@endif

				@if(isset($status) && $status == 4)
				<b>Có {{ count($data) }} đơn hàng đã hủy</b>
				 	<tr>
						<th>Mã đơn hàng</th>
						<th>Tổng số sản phẩm bán</th>
						<th>Tổng số tiền nhập</th>
						<th>Tổng số tiền bán</th>
				    </tr>
				    <?php $sum = 0; ?>
				    @foreach($data as $item)
				    <?php $sum_quantity = 0; $sum_sell = 0; $sum_import=0; 
				    foreach ($item->detail as $value) {
						$sum_quantity += $value->quantity;
						$sum_import += $value->quantity*$value->price_import;
						$sum_sell += $value->quantity*$value->price_sell;
						$sum += ($value->quantity*$value->price_sell)-($value->quantity*$value->price_import);
					}
						?>
				    <tr>
						<td>EP-{{ $item->id }}</td>
						<td>{{ $sum_quantity }}</td>
						<td>{{ number_format($sum_import,0) }} VNĐ</td>
						<td>{{ number_format($sum_sell,0 )}} VNĐ</td>
				    </tr>
				    @endforeach
			    @endif

			    @if(isset($status) && $status == 6)
				<b>Danh sách <span style="color: red;">10</span> sản phẩm được mua nhiều nhất</b>
				 	<tr>
						<th>Mã SP</th>
						<th>Tên SP</th>
						<th>Lượt mua</th>
				    </tr>
				    
				   @foreach($data as $item)
				    <tr>
						<td>P-{{ $item->product_id }}</td>
						<td>{{ $item->product->name }}</td>
						<td>{{ $item->sum }}</td>
				    </tr>
				    @endforeach
			    @endif
				</table> <hr>	
				@if(count($data) > 0))
				<center><button class=" btn btn-primary print_hidden" onclick="prin();" id="print"><i class="fa fa-print" aria-hidden="true"></i> Xuất báo cáo</button></center>	
				@endif

			    </div>
			    <!-- ./box-body -->

			</div>
			<!-- ./box -->
	    </div>
	    <!-- ./col-xs-12 -->
	</div>

	<script>
		function prin(){
			window.print();
			

		}
	</script>

@endsection
