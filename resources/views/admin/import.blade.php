@extends('admin.layouts.main')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Đơn hàng nhập</h3>
                    <a id="addProduct" href="" data-toggle="modal" data-target="#modalForm" onclick="add('import')" class="btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới đơn hàng nhập</a>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover table-bordered text-center">
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Thời gian nhập</th>
                            <th>Phiếu yêu cầu nhập</th>
                            <th>Hành động</th>
                        </tr>
                        @foreach($import as $item)
                            <tr>
                                <td>IP-{{ $item->id }}</td>
                                <td>{{ date("d/m/Y",strtotime($item->date)) }}</td>
                                <td>RQ-{{ $item->request_import->id }}</td>
                                <td><a href="" class="btn btn-primary" onclick="getEdit({!! $item->id !!},'import')" href="" data-toggle="modal" data-target="#modalForm">Xem/Sửa</a>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- ./box-body -->

                <pagination>{{$import->links()}}</pagination>
            </div>
            <!-- ./box -->
        </div>
        <!-- ./col-xs-12 -->
    </div>
</div>
    <!-- Modal thêm đơn hàng -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalForm" >
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="frm" name="frm" class="form-horizontal" method="post" action="" enctype="multipart/form-data" onsubmit="return confirm('Bạn có chắc dữ liệu đã nhập đúng?');">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Phiếu yêu cầu nhập hàng</label>
                            <div class="col-sm-10">
                               <select class="form-control" name="request_import" id="request_import">
                                   <option value="">Chọn phiếu yêu cầu nhập hàng</option>
                                   @foreach($request as $re)
                                    <option value="{{ $re->id }}">RQ-{{ $re->id }}</option>
                                   @endforeach
                               </select>
                               <span id="provider" style="color:red;" class="hidden"></span>
                            </div>
                        </div>
                        <div class="product">
                            <div class="form-group ">
                                <label for="" class="col-sm-2 control-label">Danh sách nhập</label>
                                <div class="col-sm-10" id="listProduct">
                                </div>
                                <!-- <div class="form-group">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9" style="padding-left: 23px;">
                                        <a href="javascript:void(0)" onclick="addList()"><i class="fa fa-plus-square" aria-hidden="true"></i> Thêm</a>
                                    </div>
                                </div> -->
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-sm-3">
                                <button class="btn btn-success" type="submit" style="margin: 10px 0;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
                            </label>
                        </div>
                        
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
@endsection
