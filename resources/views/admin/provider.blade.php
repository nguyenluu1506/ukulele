@extends('admin.layouts.main')
@section('content')
	<div class="row">
	    <div class="col-xs-12">
			<div class="box">
			    <div class="box-header">
				<h3 class="box-title">Nhà cung cấp</h3>
				<a id="add" href="" data-toggle="modal" data-target="#modalForm" onclick="add('provider')" class="btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
				<div class="box-tools">
				    <div class="input-group input-group-sm" style="width: 150px;">
					<input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">
					
					<div class="input-group-btn">
					    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
				    </div>
				</div>
			    </div>
			    <!-- /.box-header -->
			    <div class="box-body table-responsive no-padding">
				
				<table class="table table-hover table-bordered text-center">
				    <tr>
						<th>Mã NCC</th>
						<th>Tên nhà cung cấp</th>
						<th>Số điện thoại</th>
						<th>Địa chỉ</th>
				    </tr>
				    @foreach($provider as $item)
				    <tr>
						<td>NCC-{{ $item->id }}</td>
						<td>{{ $item->name }}</td>
						<td>{{ $item->phone }}</td>
						<td>{{ $item->address }}</td>
						<td>
							<a class="btn btn-primary btn-xs " onclick="getEdit({!! $item->id !!},'provider')" href="" data-toggle="modal" data-target="#modalForm">Sửa</a>
							<a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="confirmDel({!! $item->id !!},'provider')">Xóa</a>
						</td>
				    </tr>
				    @endforeach
				</table>		
			    </div>
			    <!-- ./box-body -->

			    <pagination>{{$provider->links()}}</pagination>
			</div>
			<!-- ./box -->
	    </div>
	    <!-- ./col-xs-12 -->
	</div>

	<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalForm" >
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"></h4>
	    </div>
	    <div class="modal-body">
			<form id="frm" name="frm" class="form-horizontal" method="post" action="" enctype="multipart/form-data" >
				{{ csrf_field() }}
			    <div class="form-group">
					<label for="" class="col-sm-3 control-label">Tên nhà cung cấp</label>
					<div class="col-sm-9">
					    <input type="text" class="form-control" id="name" name="name" placeholder="" required="true" />
					</div>
			    </div>
			    <div class="form-group">
					<label for="" class="col-sm-3 control-label">Số điện thoại</label>
					<div class="col-sm-9">
					    <input type="text" class="form-control" id="phone" name="phone" placeholder="" required="true" />
					</div>
			    </div>
			    <div class="form-group">
					<label for="" class="col-sm-3 control-label">Địa chỉ</label>
					<div class="col-sm-9">
					    <input type="text" class="form-control" id="address" name="address" placeholder="" required="true" />
					</div>
			    </div>

			    <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
			</form>
	    </div>
	</div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- <script type="text/javascript">
	
</script> -->
@endsection
