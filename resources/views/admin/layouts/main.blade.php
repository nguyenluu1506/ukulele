<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="images/favicon.ico">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- <meta http-equiv="refresh" content="5" > -->
        <title>TNGuitar-Admin</title>
        <base href="{{asset('')}}">
        <!-- Styles -->
        <link rel="stylesheet" href="admin_asset/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="admin_asset/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="admin_asset/bower_components/Ionicons/css/ionicons.min.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="admin_asset/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="admin_asset/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="admin_asset/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="admin_asset/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="admin_asset/dist/css/skins/_all-skins.min.css">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="admin_asset/dist/summernote.css">
        <link rel="stylesheet" href="admin_asset/css/print.css" type="text/css" media="print" />
    </head>

    <body class="hold-transition skin-blue sidebar-mini" ng-app="app">
        <div class="wrapper">
            @include('admin.layouts.header')
            @include('admin.layouts.leftSide')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Page Content -->
                @yield('content')
                <!-- /#page-wrapper -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                  <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2018 ThaoNguyenGuitar.</strong>
            </footer>
            @include('admin.layouts.controlSidebar')
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->
        <!-- jQuery 3 -->
        <script src="admin_asset/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="admin_asset/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Excel -->
        <!-- <script src="js/jquery.table2excel.min.js"></script>
        <script src="js/excel.js"></script> -->

        
        <!-- <script src="/vendor/laravel-filemanager/js/lfm.js"></script> -->
        <!-- TinyMCE -->
        <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>


        <!-- bootstrap datepicker -->
        <script src="admin_asset/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="admin_asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- InputMask -->
        <script src="admin_asset/plugins/input-mask/jquery.inputmask.bundle.js"></script>
        <!-- date-range-picker -->
        <script src="admin_asset/bower_components/moment/min/moment.min.js"></script>
        <script src="admin_asset/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- SlimScroll -->
        <script src="admin_asset/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="admin_asset/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="admin_asset/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="admin_asset/dist/js/demo.js"></script>
        <script src="admin_asset/js/custom.js"></script>
        <script src="js/main.js"></script>
         <script src="js/notify.min.js"></script>

         <!-- include summernote css/js -->
        <!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script> -->
        
        <script src="admin_asset/dist/summernote.js"></script>
        <script>
            $(document).ready(function() {
                $("#summernote").summernote({
                    tabsize: 2,
                    height: 200
                  });
            }); 
            var postForm = function() {
                var content = $('textarea[name="description"]').html($('#summernote').code());
            }
        </script>
        @if(isset($b))
        <script type="text/javascript">
            $(document).ready(function() {
                 $("#modalForm").modal('show');

            });
               
            </script>
        @endif

        @if(isset($c))
        <script type="text/javascript">
            $(document).ready(function() {
                 $("#modalForm").modal('show');
            });
               
            </script>
        @endif

        <script type="text/javascript">
            Date.monthNames = Date.monthNames || function( ) {
                    var arrMonth = [],
                        dateRef = new Date(),
                        year = dateRef.getFullYear(),
                        // Firefox don't support parametres, so we construct option to conform to Firefox format
                        options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};

                    dateRef.setMonth(0);
                    dateRef.setDate(10); // Eviter la fin de mois !!!
                    while (year == dateRef.getFullYear()) {
                        /* push le mois en lettre et passe au mois suivant */
                        arrMonth.push( (dateRef.toLocaleString("en-GB", options).split(' '))[2] );
                        dateRef.setMonth( dateRef.getMonth() + 1);
                    }
                    
                    return arrMonth;
                }

                /**
                 * Pour Instancier plusieurs month-picker par page,
                 * on a besoin d'une fabrique ...
                 */
                var monthPickerFactory = (function ( document ) {
                    var 
                        /**
                         * Crétion d'une instance de monthPicker
                         * @element inputElt :  un element input
                         */
                        monthPicker =  function ( inputElt, options ) {
                            var valeur = inputElt.value,
                                tabVal = valeur.split('/'), // Voir un regex pour généraliser la construction 
                                b_mp = document.createElement('b'),
                                span_mp = b_mp.appendChild( document.createElement('span') ),
                                slct_year = span_mp.appendChild( document.createElement('select') ),
                                /**
                                 * clickBtnMois
                                 * reponse à un click sur un bouton mois
                                 * @event e : 
                                 */
                                clickBtnMois = function( e ) {
                                    var val = e.target.value,
                                        label = e.target.parentNode,
                                        labelNodeList = label.parentNode.getElementsByTagName('label');
                                    
                                    if(val == 'on') { // compatibilité Opera

                                        for (var i = 0; i < labelNodeList.length; ++i) {
                                            if( label == labelNodeList[i] ) val = i + 1;
                                        }
                                    }
                                    tabVal = [ val, slct_year.value ];
                                    inputElt.value = '' + tabVal[0]  + '/' + tabVal[1];
                                     
                                    return;
                                },          
                                selectAnnee = function( e ) {
                                    tabVal[1] = slct_year.value;
                                    inputElt.value = '' + tabVal[0]  + '/' + tabVal[1];
                                },
                                fillSelect = function( an ) {
                                    an = an || (new Date()).getFullYear();
                                    for(var i = an - 3 ; i < an + 10 ; i++) {
                                        slct_year.appendChild( document.createElement('option') ).textContent = i;
                                        if( an == i ) {
                                            slct_year.lastChild.setAttribute( 'selected', 'selected' );
                                        }
                                    }
                                    slct_year.addEventListener( 'change', selectAnnee );
                                    
                                },
                                fillMois = function( mois, tabMois ) {
                                    tabMois = tabMois || Date.monthNames().map( function(str) { return ( str.length > 4 ) ? str.substr(0 , 3) + '.' : str; 
                                    } );
                                    mois = mois || (new Date()).getMonth() + 1;
                                    
                                    for(var i = 0, lbl_mois, rd_mois ; i < tabMois.length ; i++) {
                                        lbl_mois  = document.createElement('label'),
                                        rd_mois = lbl_mois.appendChild( document.createElement('input') );
                                            
                                            
                                        rd_mois.value = i + 1;
                                        rd_mois.setAttribute( 'type', 'radio');
                                        rd_mois.setAttribute( 'name', 'mois');
                                        rd_mois.addEventListener( 'change', clickBtnMois );
                                        rd_mois.checked = ( mois == i + 1 );
                                        if( rd_mois.value == '' && mois == i + 1 ) {
                                            rd_mois.value = 'on';
                                        }
                                        
                                        lbl_mois.appendChild( document.createElement('span') ).textContent = tabMois[i];
                                        
                                        span_mp.appendChild( lbl_mois );
                                    }
                                    return;
                                };
                                
                            b_mp.classList.add( 'month-picker' );

                            inputElt.classList.add( 'month-picker' );
                            fillSelect( ( tabVal.length > 1 ) ? parseInt( tabVal[1] ) : null );
                            span_mp.appendChild( document.createElement('br') );
                            fillMois( ( tabVal.length ) ? parseInt( tabVal[0] ) : null );
                            
                            inputElt.parentNode.insertBefore(b_mp, inputElt);
                            b_mp.insertBefore(inputElt, span_mp);
                            
                            return b_mp;
                        };

                    return {
                        createMonthPicker : monthPicker
                    }
                })(window.document);

                // Start
                monthPickerFactory.createMonthPicker( document.getElementById('mois') );
            </script>

            <script>
                var time = new Date().getTime();
                $(document.body).bind("mousemove keypress", function(e) {
                    time = new Date().getTime();
                });

                function refresh() {
                    if(new Date().getTime() - time >= 60000) 
                        window.location.reload(true);
                    else 
                        setTimeout(refresh, 10000);
                }

                setTimeout(refresh, 10000);
            </script>
    </body>

</html>
