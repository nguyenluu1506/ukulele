<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="admin_asset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::guard('admin')->user()->name }}</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form> -->
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" >
      <li >
  			<a href="{{ route('admin.brand') }}">
  			    <i class="fa fa-link" aria-hidden="true"></i><span>Thương hiệu</span>
  			</a>
	    </li>
      <li>
  			<a href="{{ route('admin.category') }}">
  			    <i class="fa fa-bars" aria-hidden="true"></i> <span>Ngành hàng</span>
  			</a>
	    </li>
      <li>
  			<a href="{{ route('admin.product') }}">
  			    <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Sản phẩm</span>
  			</a>
	    </li>
      <li>
  			<a href="{{ route('admin.user') }}">
  			    <i class="fa fa-user" aria-hidden="true"></i> <span>Tài khoản</span>
  			</a>
	    </li>
      <li>
  			<a href="{{ route('admin.export') }}">
  			    <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Đơn hàng bán</span>
  			</a>
	    </li>
      <li>
        <a href="{{ route('admin.provider') }}">
            <i class="fa fa-user" aria-hidden="true"></i> <span>Nhà cung cấp</span>
        </a>
      </li>
      <li>
        <a href="{{ route('admin.import') }}">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Đơn hàng nhập</span>
        </a>
      </li>
      <li>
        <a href="{{ route('admin.request') }}">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Phiếu yêu cầu nhập</span>
        </a>
      </li>
      <!-- <li>
        <a href="#">
            <i class="fa fa-percent" aria-hidden="true"></i> <span>Giảm giá</span>
        </a>
      </li> -->
      <li>
  			<a href="{{ route('admin.slider') }}">
  			    <i class="fa fa-sliders" aria-hidden="true"></i> <span>Banner</span>
  			</a>
	    </li>
      <li>
        <a href="{{ route('admin.report') }}">
            <i class="fa fa-sliders" aria-hidden="true"></i> <span>Báo cáo</span>
        </a>
      </li>
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
