@extends('admin.layouts.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sản phẩm</h3>
                    <a id="addProduct" href="" data-toggle="modal" data-target="#modalForm" onclick="add('product')" class="btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover table-bordered text-center">
                        <tr>
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>Mô tả</th>
                            <th>Số lượng</th>
                            <th>Giá nhập (VNĐ)</th>
                            <th>Giá bán (VNĐ)</th>
                            <th>Thương hiệu</th>
                            <th>Danh mục</th>
                            <th>Số người đánh giá</th>
                            <th>Đánh giá trung bình</th>
                            <th>Tags</th>
                            <th>Hành động</th>
                        </tr>
                        @foreach($product as $item)
                            <tr>
                                <td>PD-{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{!! substr($item->description,0,400) !!}...</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ number_format($item->price_import,0) }}</td>
                                <td>{{ number_format($item->price_sell,0) }}</td>
                                <td>{{ $item->brand->name }}</td>
                                <td>{{ $item->category->name }}</td>
                                <td>{{ $item->count_rating }}</td>
                                <td>
                                    <?php  
                                        for($i = 1; $i<= $item->rating; $i++){
                                        ?>
                                            <i name="" class="fa fa-star" style="color: orange;"></i>
                                            <?php
                                        }
                                        for($i = 1; $i<= (5- $item->rating); $i++){
                                        ?>
                                            <i name="" class="fa fa-star-o"></i>
                                            <?php
                                        }  
                                    ?>
                                </td>
                                <td>{{ $item->tag }}</td>
                                <td>
                                    <div class="status">
                                        @if($item->status == 1)
                                        <a class="btn btn-xs btn-info" href="javascript:void(0);" onclick="cf_Active({!! $item->id !!},{!! $item->status !!})">Ẩn</a>
                                        @else
                                            <a class="btn btn-xs btn-success" href="javascript:void(0);" onclick="cf_Active({!! $item->id !!},{!! $item->status !!})">Hiện</a>
                                        @endif
                                    </div>
                                    
                                    <a class="btn btn-primary btn-xs " onclick="getEdit({!! $item->id !!},'product')" href="" data-toggle="modal" data-target="#modalForm">Sửa</a>
                                    <!-- <a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="confirmDel({!! $item->id !!},'product')">Không</a> -->
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- ./box-body -->

                <pagination>{{$product->links()}}</pagination>
            </div>
            <!-- ./box -->
        </div>
        <!-- ./col-xs-12 -->
    </div>

    <!-- Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalForm" onsubmit="return postForm()" style="overflow: auto !important;">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="frm" name="frm" class="form-horizontal" method="post" action="" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Thương hiệu</label>
                            <div class="col-sm-9">
                                <select name="brand" id="brand" class="form-control">
                                    <option value="">Chọn thương hiệu</option>
                                    @foreach($brand as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Danh mục</label>
                            <div class="col-sm-9">
                                <select name="category" id="category" class="form-control">
                                    <option value="">Chọn danh mục</option>
                                    @foreach($category as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Tên sản phẩm</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="" required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Mô tả</label>
                            <div class="col-sm-9">
                                <textarea name="description" id="summernote"  required="true" id="description" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Số lượng</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="" required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Giá nhập (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="price_import" name="price_import" placeholder="" required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Giá bán (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="price_sell" name="price_sell" placeholder="" required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Ảnh đại diện SP</label>
                            <div class="col-sm-9">
                                <label class="btn btn-success btn-file add_image">
                                Chọn ảnh <input type="file" name="product_avt" id="file" style="display: none;" accept="image/*"  onchange="readURL(this);">
                                </label><br><br>
                                <img id="view_Img" src="" alt="your image" hidden="true"  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Ảnh chi tiết SP</label>
                            <div id="product_img" class="col-sm-9">
                                <label class="add_img btn btn-success" style="margin-bottom: 10px;"><i class="glyphicon glyphicon-plus-circle"></i> Thêm ảnh</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Tags</label>
                            <div class="form-check form-check-inline col-sm-3">
                                <input class="form-check-input" type="checkbox" id="children" name="tag[]" value="#child">
                                <label class="form-check-label" for="children">Trẻ em</label>
                            </div>
                            <div class="form-check form-check-inline col-sm-3">
                                <input class="form-check-input" type="checkbox" id="adults" name="tag[]" value="#adults">
                                <label class="form-check-label" for="adults">Người lớn</label>
                            </div> 
                            <div class="form-check form-check-inline col-sm-3">
                                <input class="form-check-input" type="checkbox" id="normal" name="tag[]" value="#normal">
                                <label class="form-check-label" for="normal">SP phổ thông</label>
                            </div> 
                            <div class="form-check form-check-inline col-sm-3">
                                <input class="form-check-input" type="checkbox" id="high" name="tag[]" value="#high">
                                <label class="form-check-label" for="high">SP cao cấp</label>
                            </div>  
                        </div>
            
                        <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- <script type="text/javascript">

    </script> -->
@endsection
<style type="text/css">

    .col-md-2{
        padding:0px !important;;
    }
     .thum {
        margin:0px 1px;
        width:200px;
        height: 135px;
        border: 1px solid #000;
        } 
    .remove_img_preview {
        z-index: 9999;
        position:relative;
        top: -148px;
        float: right;
        background:orange;
        color:white;
        border-radius:50px;
        font-size:0.9em;
        padding: 1px 7px;
        text-align:center;
        cursor:pointer;
    }
    .remove_img_preview:before {
        content: "-";
        font-weight: bold;
    }
    .modal-content{
        padding:10px;
    }
</style>