@extends('admin.layouts.main')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Đơn hàng bán</h3>
                    <a id="addProduct" href="" data-toggle="modal" data-target="#modalForm" onclick="add('export')" class="btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover table-bordered text-center">
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Người đặt</th>
                            <th>Tên người nhận</th>
                            <th>SĐT người nhận</th>
                            <th>Địa chỉ người nhận</th>
                            <th>Thời gian đặt</th>
                            <th>Thông tin thanh toán</th>
                            <th>Mã vận đơn</th>
                            <!-- <th>Mã giảm giá</th> -->
                            <th>Trạng thái đơn hàng</th>
                            <th>Hành động</th>
                        </tr>
                        @foreach($export as $item)
                            <tr>
                                <td>EP-{{ $item->id }}</td>
                                <td>{{ $item->name_reciever }}</td>
                                <td>{{ $item->name_reciever }}</td>
                                <td>{{ $item->phone_reciever }}</td>
                                <td>{{ $item->address_reciever }}</td>
                                <td>{{ $item->date_order }}</td>
                                <td>
                                    <div class="payment">
                                            @if($item->payment == "khi-nhan-hang")
                                            Thanh toán khi nhận hàng
                                            @endif
                                            @if($item->payment == "chuyen-khoan")
                                                Chuyển khoản
                                            @endif
                                            @if($item->payment == "ngan-luong")
                                                Thanh toán qua Ngân lượng
                                            @endif
                                            @if($item->payment == "tien-mat")
                                                Tiền mặt
                                            @endif
                                    </div>
                                </td>
                                <td>@if($item->order_tracking !=null)
                                    {{ $item->order_tracking }} <br>
                                    
                                    <a href="http://track.ghn.vn/order/tracking?code={{$item->order_tracking}}" target="blank">Xem</a></td>
                                    @endif
                               <!--  <td>{{ $item->sale_id }}</td> -->
                                <td>
                                    <div class="status">
                                        @if($item->status == 0)
                                        <b>Đang chờ xác nhận</b> <a class="btn btn-xs btn-info" href="{{ route('admin.export.cf_ActiveExport',$item->id) }}">Xác nhận ngay?</a>
                                        @endif
                                        @if($item->status == 1)
                                            Đơn hàng đã được xác nhận
                                        @endif
                                        @if($item->status == 2)
                                            Thanh toán thành công
                                        @endif
                                        @if($item->status == 3)
                                            Đã giao hàng, đã thanh toán
                                        @endif
                                        @if($item->status == 4)
                                            Hủy đơn hàng
                                        @endif
                                        @if($item->status == 5)
                                            Đang giao hàng
                                        @endif
                                    </div>
                                </td>
                                <td><a href="" onclick="getEdit({!! $item->id !!},'export')" class="btn btn-primary" href="" data-toggle="modal" data-target="#modalForm">Xem/Sửa</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- ./box-body -->

                <pagination>{{$export->links()}}</pagination>
            </div>
            <!-- ./box -->
        </div>
        <!-- ./col-xs-12 -->
    </div>

    <!-- Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalForm" >
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="frm" name="frm" class="form-horizontal" method="post" action="" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Tên khách hàng (*)</label>
                            <div class="col-sm-9">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">Khách quen</a></li>
                                    <li><a data-toggle="tab" id="new" href="#menu1">Khách mới</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active" style="margin-top: 10px;">
                                        <select name="user_old"  id="user_old" class="form-control">
                                            <option value="">Chọn khách hàng</option>
                                            @foreach($user as $u)
                                            <option value="{{ $u->id }}">{{ $u->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div id="menu1" class="tab-pane fade" style="margin-top: 10px;">

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Tên khách hàng (*)</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="new_user_name"  name="new_user_name" placeholder=""  />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">SĐT (*)</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="new_user_phone"  name="new_user_phone" placeholder=""  />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Địa chỉ (*)</label>
                                            <div class="col-sm-10">
                                                <input type="text"  class="form-control" id="new_user_address" name="new_user_address" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                        <div class="product">
                            <div class="form-group ">
                                <label for="" class="col-sm-3 control-label">Danh sách nhập</label>
                                <div class="col-sm-9" id="listProduct">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-control arrayProduct"  name="product_id[]" id="" class="getQty" required>
                                                <option value="" readonly="true">Chọn sản phẩm</option>
                                                @foreach($product as $pro)
                                                <option value="{{ $pro->id }}">{{ $pro->name }} <i>(Số lượng trong kho: {{$pro->quantity}})</i></option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" id="quantity" name="quantity[]" placeholder="Nhập số lượng" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9" id="them">
                                            <a href="javascript:void(0)" onclick="addList()"><i class="fa fa-plus-square" aria-hidden="true"></i> Thêm</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="phone">
                            <label for="" class="col-sm-3 control-label">SĐT nhận hàng</label>
                            <div class="col-sm-9" >
                                <input type="text" class="form-control" id="phone_reciever" name="phone_reciever" placeholder="Số điện thoại nhận" width="100%">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="" class="col-sm-3 control-label">Địa chỉ nhận hàng</label>
                            <div class="col-sm-9" >
                                <input type="text" class="form-control" id="address" name="address" placeholder="Địa chỉ nhận" width="100%">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="" class="col-sm-3 control-label">Thông tin thanh toán</label>
                            <div class="col-sm-9" >
                               <div class="controls" style="margin: 10px 0;">
                                    <input class="" type="radio" name="payment_name" id="tien-mat" value="tien-mat" placeholder="" required="" checked >
                                    <label for="khi-nhan-hang">Thanh toán tiền mặt</label> <br>
                                    <input class="" type="radio" name="payment_name" id="khi-nhan-hang" value="khi-nhan-hang" placeholder="" required="" >
                                    <label for="khi-nhan-hang">Thanh toán khi nhận hàng</label> <br>
                                    

                                    <input class="" type="radio" name="payment_name" id="chuyen-khoan" value="chuyen-khoan" placeholder="" required="" data-toggle="collapse" data-target="#chuyenkhoan">
                                    <label for="chuyen-khoan">Thanh toán chuyển khoản trực tiếp</label> <br>
                                    <input class="" type="radio" name="payment_name" id="ngan-luong" value="ngan-luong" placeholder="" required="" data-toggle="collapse" data-target="#nganluong">
                                    <label for="ngan-luong">Thanh toán với Ngân Lượng</label> <br>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden" id="order_status">
                            <label for="" class="col-sm-3 control-label">Tình trạng đơn hàng</label>
                            <div class="col-sm-9" >
                                <select name="status" id="status" class="form-control">
                                    <option value="">Chọn</option>
                                    <option value="0">Đang chờ xác nhận</option>
                                    <option value="1">Đặt hàng thành công</option>
                                    <option value="2">Thanh toán thành công</option>
                                    <option value="3">Đơn hàng thành công</option>
                                    <option value="5">Đang giao hàng</option>
                                    <option value="4">Hủy đơn hàng</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group ">
                            <label for="" class="col-sm-3 control-label">Mã giảm giá</label>
                            <div class="col-sm-9" id="address">
                                <input type="text" class="form-control" id="address" name="address" placeholder="Địa chỉ nhận" width="100%">
                            </div>
                        </div> -->
                        <div class="form-group hidden" id="tracking">
                            <label for="" class="col-sm-3 control-label">Mã vận đơn</label>
                            <div class="col-sm-9" >
                                <input type="text" class="form-control" id="order_tracking" name="order_tracking" placeholder="Mã vận đơn" width="100%">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label col-sm-3">
                                <button class="btn btn-success" type="submit" style="margin-top: 20px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
                            </label>
                        </div>
                        
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


   <!--   Modal
       <div class="modal fade" tabindex="-1" role="dialog" id="modalDetail" >
      <div class="modal-dialog" style="width: 80%">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Chi tiết nhập</h4>
              </div>
              <div class="modal-body">
                  <table class="table table-hover table-bordered text-center">
                      <tr>
                          <td>Thời gian lập hóa đơn</td>
                          <td id="times"></td>
                      </tr>
                      <tr>
                          <td>Khách hàng</td>
                          <td id="user_get"></td>
                      </tr>
                      <tr>
                          <td>SĐT</td>
                          <td id="phone"></td>
                      </tr>
                      <tr>
                          <td>Địa chỉ nhận</td>
                          <td id="address"></td>
                      </tr>
                      <tr>
                          <td>Danh sách sản phẩm</td>
                          <td>
                              <table class="table table-hover table-bordered text-center">
                                  <thead >
                                      <tr>
                                          <th id="">Tên sản phẩm</th>
                                          <th id="">Số lượng</th>
                                          <th id="">Giá nhập</th>
                                          <th id="">Giá bán</th>
                                          <th id="">Thành tiền</th>
                                      </tr>
                                  </thead>
                                  <tbody id="product_get">
                                  </tbody>
                              </table>
                          </td>
                      </tr>
                      
                  </table>
              </div>
          </div>/.modal-content
      </div>/.modal-dialog
       </div>/.modal -->
    
@endsection
