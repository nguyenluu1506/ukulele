@extends('admin.layouts.main')
@section('content')
	<div class="row">
	    <div class="col-xs-12">
			<div class="box">
			    <div class="box-header">
				<h3 class="box-title">Slider</h3>
				<a id="add" href="" data-toggle="modal" data-target="#modalForm" onclick="add('slider')" class="btn-primary btn-xs"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
				<div class="box-tools">
				    <div class="input-group input-group-sm" style="width: 150px;">
					<input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">
					
					<div class="input-group-btn">
					    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
				    </div>
				</div>
			    </div>
			    <!-- /.box-header -->
			    <div class="box-body table-responsive no-padding">
				
				<table class="table table-hover table-bordered text-center">
				    <tr>
						<th>#</th>
						<th>Ảnh</th>
						<th>Tiêu đề</th>
						<th>Trạng thái</th>
				    </tr>
				    @foreach($slider as $item)
				    <tr>
						<td>{{ $item->id }}</td>
						<td><img src="{{ $item->url }}" width="200px" height="auto"></td>
						<td>{{ $item->title }}</td>
						<td>
							@if($item->status == 1)
							<a class="btn btn-success btn-xs " onclick="confirmActive({!! $item->id !!}, {!! $item->status !!})" href="javascript:void(0);" id="status">Đang hiện</a>
							@else
							<a class="btn btn-danger btn-xs " onclick="confirmActive({!! $item->id !!} , {!! $item->status !!})" href="javascript:void(0);" id="status">Đã ẩn</a>
							@endif
						</td>
				    </tr>
				    @endforeach
				</table>		
			    </div>
			    <!-- ./box-body -->

			    <pagination>{{$slider->links()}}</pagination>
			</div>
			<!-- ./box -->
	    </div>
	    <!-- ./col-xs-12 -->
	</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalForm" >
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"></h4>
	    </div>
	    <div class="modal-body">
			<form id="frm" name="frm" class="form-horizontal" method="post" action="" enctype="multipart/form-data" >
				{{ csrf_field() }}
			    <div class="form-group">
				<label for="" class="col-sm-3 control-label">Tiêu đề</label>
				<div class="col-sm-9">
				    <input type="text" class="form-control" id="title" name="title" placeholder="" required="true" />
				</div>
			    </div>
			    
			    <div class="form-group">
					<label for="" class="col-sm-3 control-label">Ảnh Slider</label>
					<div class="col-sm-9">
					    <label class="btn btn-primary btn-file add_image">
			            Chọn ảnh <input type="file" name="file" id="file" style="display: none;" accept="image/*"  onchange="readURL(this);">
		              	</label><br><br>
		              	<img id="view_Img" src="" alt="your image" hidden="true"  />
					</div>
			    </div>

			    <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Lưu</button>
			</form>
	    </div>
	</div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- <script type="text/javascript">
	
</script> -->
@endsection
