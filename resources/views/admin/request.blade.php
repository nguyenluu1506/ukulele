@extends('admin.layouts.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Phiếu yêu cầu nhập hàng</h3>
                    <a id="addProduct" href="" data-toggle="modal" data-target="#modalRequest" onclick="add('request')" class="btn-primary btn-xs text-right"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới phiếu yêu cầu nhập hàng</a>
                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" ng-model="searchkey" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover table-bordered text-center">
                        <tr>
                            <th>Mã phiếu yêu cầu</th>
                            <th>Nhà cung cấp</th>
                            <th>Danh sách yêu cầu nhập</th>
                            <th>Tình trạng</th>
                            <th>Hành động</th>
                        </tr>
                        @foreach($request as $item)
                            <tr>
                                <td>RQ-{{ $item->id }}</td>
                                <td>{{ $item->provider->name }}</td>
                                <td><table class="table table-bordered">
                                    <tr>
                                        <th>STT</th>
                                        <th>Sản phẩm</th>
                                        <th>Số lượng</th>
                                    </tr>
                                    <?php $count = 0; ?>
                                    @foreach($item->product as $key => $val)
                                    <?php if($count == 5){ echo "<td>...</td><td>...</td><td>...</td>"; break; } ?>
                                    @foreach($item->detail as $key => $val2) 
                                    @if($val->id == $val2->product_id)

                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $val->name }}</td> 
                                        <td>{{ $val2->quantity }}</td>
                                    </tr> 
                                    <?php $count++; ?>
                                    
                                    @endif
                                    @endforeach
                                    @endforeach</table>
                                </td>
                                <td>
                                    @if($item->status ==0)
                                    Hàng chưa về
                                    @else 
                                    Hàng đã nhập kho
                                    @endif
                                </td>
                                <td><a class="btn btn-primary btn-xs " onclick="getEdit({!! $item->id !!},'request')" href="" data-toggle="modal" data-target="#modalRequest">Xem/Sửa</a>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- ./box-body -->

                <pagination>{{$request->links()}}</pagination>
            </div>
            <!-- ./box -->
        </div>
        <!-- ./col-xs-12 -->
    </div>

    
    
    <!-- Modal phiếu y/c nhập hàng -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalRequest" >
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="frm" name="frm" class="form-horizontal" method="post" action="{{ route('admin.request.postAdd') }}" enctype="multipart/form-data" onsubmit="return confirm('Bạn có chắc dữ liệu đã nhập đúng?');">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">Nhà cung cấp</label>
                            <div class="col-sm-9">
                               <select class="form-control" name="provider" id="provider" required>
                                   <option value="">Chọn nhà cung cấp</option>
                                   @foreach($provider as $pr)
                                    <option value="{{ $pr->id }}">{{ $pr->name }}</option>
                                   @endforeach
                               </select>
                            </div>
                        </div>
                        <div class="product">
                            <div class="form-group ">
                                <label for="" class="col-sm-3 control-label">Danh sách nhập</label>
                                <div class="col-sm-9" id="listProduct">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-control arrayProduct"  name="product_id[]" id="" class="getQty" required>
                                                <option value="" readonly="true">Chọn sản phẩm</option>
                                                @foreach($product as $pro)
                                                <option value="{{ $pro->id }}">{{ $pro->name }} <i>(Số lượng trong kho: {{$pro->quantity}})</i></option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" id="quantity" name="quantity[]" placeholder="Nhập số lượng" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9" id="them">
                                            <a href="javascript:void(0)" onclick="addList()"><i class="fa fa-plus-square" aria-hidden="true"></i> Thêm</a>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            
                            <!-- <a id="add-product-request" style="margin-top: 5px; float: right;" href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nhập tiếp</a> -->
                        </div>
                        <div class="form-group" id="btnAdd" style="text-align: center;">
                            <button   class="btn btn-success" type="submit" style="margin: 10px 0;"><i class="fa fa-floppy-o"  aria-hidden="true"></i> Gửi yêu cầu nhập hàng</button>
                        </div>
                        
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
